class Scene {
  constructor (clearColor) {
    this.clearColor = { r: clearColor[0], g: clearColor[1], b: clearColor[2], a: clearColor[3] }
    this.meshes = []
  }
  addMesh (mesh) {
    this.meshes.push(mesh)
  }
}

module.exports = Scene
