const Mesh = require('./Mesh')
const WebGLRenderer = require('./WebGLRenderer')
const Scene = require('./Scene')
const PerspectiveCamera = require('./camera/PerspectiveCamera')
const OrthographicCamera = require('./camera/OrthographicCamera')
const Grid = require('./geometries/Grid')
const Sphere = require('./geometries/Sphere')
const Cone = require('./geometries/Cone')
const Cilinder = require('./geometries/Cilinder')
const Cube = require('./geometries/Cube')
const { AxisX, AxisY, AxisZ } = require('./geometries/Axis')
const { rgba, degToRad, disableRightClickContextMenu, unormalize } = require('./utils/utils')

const { state } = require('./gui/gui')

const canvas = document.getElementById('c')
const gl = canvas.getContext('webgl')
disableRightClickContextMenu(canvas) // for mouse events

// Render
function render () {
  // Camera setup
  let camera
  if (state.camera.perspective) {
    camera = new PerspectiveCamera(degToRad(state.camera.fovy), state.camera.aspect, state.camera.near, state.camera.far)
  } else {
    camera = new OrthographicCamera(state.camera.left, state.camera.right, state.camera.bottom, state.camera.top, state.camera.near, state.camera.far)
  }
  camera.centerX = state.camera.centerX
  camera.centerY = state.camera.centerY
  camera.centerZ = state.camera.centerZ
  camera.eyeX = state.camera.eyeX
  camera.eyeY = state.camera.eyeY
  camera.eyeZ = state.camera.eyeZ
  camera.upX = state.camera.upX
  camera.upY = state.camera.upY
  camera.upZ = state.camera.upZ
  camera.fovy = degToRad(state.camera.fovy)
  camera.aspect = state.camera.aspect
  camera.near = state.camera.near
  camera.far = state.camera.far

  // Grid setup
  const grid = new Grid(state.settings.gridSize)
  const gridMaterial = new Float32Array(rgba(state.settings.gridColor))
  const gridMesh = new Mesh(grid, gridMaterial, gl.LINES)

  // Axis x setup
  const xAxis = new AxisX(state.settings.gridSize / 2)
  const xAxisMaterial = new Float32Array(rgba(state.settings.xAxisColor))
  const xAxisMesh = new Mesh(xAxis, xAxisMaterial, gl.LINES)

  // Axis y setup
  const yAxis = new AxisY(state.settings.gridSize / 2)
  const yAxisMaterial = new Float32Array(rgba(state.settings.yAxisColor))
  const yAxisMesh = new Mesh(yAxis, yAxisMaterial, gl.LINES)

  // Axis z setup
  const zAxis = new AxisZ(state.settings.gridSize / 2)
  const zAxisMaterial = new Float32Array(rgba(state.settings.zAxisColor))
  const zAxisMesh = new Mesh(zAxis, zAxisMaterial, gl.LINES)

  // Sphere setup
  const sphere = new Sphere()
  const sphereMaterial = new Float32Array(rgba(state.sphere.color))
  const sphereMesh = new Mesh(sphere, sphereMaterial, state.sphere.primitive)
  sphereMesh.tx = unormalize(state.sphere.tx, state.settings.gridSize, 0)
  sphereMesh.ty = unormalize(state.sphere.ty, state.settings.gridSize, 0)
  sphereMesh.tz = unormalize(state.sphere.tz, state.settings.gridSize, 0)
  sphereMesh.rx = degToRad(state.sphere.rx)
  sphereMesh.ry = degToRad(state.sphere.ry)
  sphereMesh.rz = degToRad(state.sphere.rz)
  sphereMesh.sx = unormalize(state.sphere.sx, state.settings.gridSize, 1)
  sphereMesh.sy = unormalize(state.sphere.sy, state.settings.gridSize, 1)
  sphereMesh.sz = unormalize(state.sphere.sz, state.settings.gridSize, 1)

  // Cone setup
  const cone = new Cone()
  const coneMaterial = new Float32Array(rgba(state.cone.color))
  const coneMesh = new Mesh(cone, coneMaterial, state.cone.primitive)
  coneMesh.tx = unormalize(state.cone.tx, state.settings.gridSize, 0)
  coneMesh.ty = unormalize(state.cone.ty, state.settings.gridSize, 0)
  coneMesh.tz = unormalize(state.cone.tz, state.settings.gridSize, 0)
  coneMesh.rx = degToRad(state.cone.rx)
  coneMesh.ry = degToRad(state.cone.ry)
  coneMesh.rz = degToRad(state.cone.rz)
  coneMesh.sx = unormalize(state.cone.sx, state.settings.gridSize, 1)
  coneMesh.sy = unormalize(state.cone.sy, state.settings.gridSize, 1)
  coneMesh.sz = unormalize(state.cone.sz, state.settings.gridSize, 1)

  // Cilinder setup
  const cilinder = new Cilinder()
  const cilinderMaterial = new Float32Array(rgba(state.cilinder.color))
  const cilinderMesh = new Mesh(cilinder, cilinderMaterial, state.cilinder.primitive)
  cilinderMesh.tx = unormalize(state.cilinder.tx, state.settings.gridSize, 0)
  cilinderMesh.ty = unormalize(state.cilinder.ty, state.settings.gridSize, 0)
  cilinderMesh.tz = unormalize(state.cilinder.tz, state.settings.gridSize, 0)
  cilinderMesh.rx = degToRad(state.cilinder.rx)
  cilinderMesh.ry = degToRad(state.cilinder.ry)
  cilinderMesh.rz = degToRad(state.cilinder.rz)
  cilinderMesh.sx = unormalize(state.cilinder.sx, state.settings.gridSize, 1)
  cilinderMesh.sy = unormalize(state.cilinder.sy, state.settings.gridSize, 1)
  cilinderMesh.sz = unormalize(state.cilinder.sz, state.settings.gridSize, 1)

  // Cube setup
  const cube = new Cube()
  const cubeMaterial = new Float32Array(rgba(state.cube.color))
  const cubeMesh = new Mesh(cube, cubeMaterial, state.cube.primitive)
  cubeMesh.tx = unormalize(state.cube.tx, state.settings.gridSize, 0)
  cubeMesh.ty = unormalize(state.cube.ty, state.settings.gridSize, 0)
  cubeMesh.tz = unormalize(state.cube.tz, state.settings.gridSize, 0)
  cubeMesh.rx = degToRad(state.cube.rx)
  cubeMesh.ry = degToRad(state.cube.ry)
  cubeMesh.rz = degToRad(state.cube.rz)
  cubeMesh.sx = unormalize(state.cube.sx, state.settings.gridSize, 1)
  cubeMesh.sy = unormalize(state.cube.sy, state.settings.gridSize, 1)
  cubeMesh.sz = unormalize(state.cube.sz, state.settings.gridSize, 1)

  // Scene
  const scene = new Scene(rgba(state.settings.backgroundColor)) // ClearColor

  // Grid add on scene
  scene.addMesh(gridMesh)

  // Axis add on scene
  scene.addMesh(xAxisMesh)
  scene.addMesh(yAxisMesh)
  scene.addMesh(zAxisMesh)

  // Meshes add on scene
  if (state.sphere.show) {
    scene.addMesh(sphereMesh)
  }
  if (state.cone.show) {
    scene.addMesh(coneMesh)
  }
  if (state.cilinder.show) {
    scene.addMesh(cilinderMesh)
  }
  if (state.cube.show) {
    scene.addMesh(cubeMesh)
  }

  // Rendering scene
  const webGLInstance = new WebGLRenderer(canvas)
  webGLInstance.render(scene, camera) // (scene, camera)

  // Rendering loop
  window.requestAnimationFrame(render)
}

/*
// Mouse events
var mouse = require('mouse-event')
// Mouse event listener
window.addEventListener('mousemove', function (ev) {
  console.log(mouse.buttons(ev), mouse.x(ev), mouse.y(ev))
})
*/

// Forst call for rendering loop
window.requestAnimationFrame(render)
