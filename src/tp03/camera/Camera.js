const glMatrix = require('gl-matrix')

class Camera {
  constructor () {
    this.projectionMatrix = undefined
    this.eyeX = undefined
    this.eyeY = undefined
    this.eyeZ = undefined
    this.centerX = undefined
    this.centerY = undefined
    this.centerZ = undefined
    this.upX = undefined
    this.upY = undefined
    this.upZ = undefined
    this.viewMatrix = glMatrix.mat4.create()
  }
  getViewMatrix () {
    glMatrix.mat4.identity(this.viewMatrix)
    glMatrix.mat4.lookAt(this.viewMatrix, [this.eyeX, this.eyeY, this.eyeZ], [this.centerX, this.centerY, this.centerZ], [this.upX, this.upY, this.upZ])
    return this.viewMatrix
  }
}

module.exports = Camera
