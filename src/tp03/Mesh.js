const glMatrix = require('gl-matrix')

class Mesh {
  constructor (geometry, material, primitive) {
    this.geometry = geometry
    this.material = material
    this.modelMatrix = glMatrix.mat4.create()
    this.primitive = primitive

    // OpenGL Vertex Buffer Object
    this.VBOData = new Float32Array(this.geometry.vertices)

    // OpenGL Index Buffer Object
    this.IBOData = new Uint16Array(this.geometry.faces)

    // Translation
    this.tx = 0
    this.ty = 0
    this.tz = 0

    // Rotation
    this.rx = 0
    this.ry = 0
    this.rz = 0

    // Scale
    this.sx = 1
    this.sy = 1
    this.sz = 1
  }
  getModelMatrix () {
    glMatrix.mat4.identity(this.modelMatrix)
    glMatrix.mat4.translate(
      this.modelMatrix,
      this.modelMatrix,
      [this.tx, this.ty, this.tz]
    )

    glMatrix.mat4.rotateX(
      this.modelMatrix,
      this.modelMatrix,
      this.rx
    )

    glMatrix.mat4.rotateY(
      this.modelMatrix,
      this.modelMatrix,
      this.ry
    )

    glMatrix.mat4.rotateZ(
      this.modelMatrix,
      this.modelMatrix,
      this.rz
    )

    glMatrix.mat4.scale(
      this.modelMatrix,
      this.modelMatrix,
      [this.sx, this.sy, this.sz]
    )
    return this.modelMatrix
  }
}

module.exports = Mesh
