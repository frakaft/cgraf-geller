const dat = require('dat.gui')
const gui = new dat.GUI()
const canvas = document.getElementById('c')
const gl = canvas.getContext('webgl')

const state = {
  settings: {
    backgroundColor: [51, 51, 51],
    xAxisColor: [255, 0, 0],
    yAxisColor: [0, 255, 0],
    zAxisColor: [0, 0, 255],
    gridColor: [225, 225, 225],
    gridSize: 10
  },
  camera: {
    perspective: true,
    centerX: 0,
    centerY: 0,
    centerZ: 0,
    eyeX: 5,
    eyeY: 5,
    eyeZ: 5,
    upX: 0,
    upY: 1,
    upZ: 0,
    fovy: 45,
    aspect: gl.canvas.width / gl.canvas.height,
    near: 0.001,
    far: 1000,
    left: -10,
    right: 10,
    bottom: -10,
    top: 10
  },
  sphere: {
    show: true,
    primitive: gl.TRIANGLES,
    color: [0, 0, 255],
    tx: 0,
    ty: 0,
    tz: 0,
    rx: 0,
    ry: 0,
    rz: 0,
    sx: 0,
    sy: 0,
    sz: 0
  },
  cone: {
    show: false,
    primitive: gl.TRIANGLES,
    color: [0, 0, 255],
    tx: 0,
    ty: 0,
    tz: 0,
    rx: 270,
    ry: 0,
    rz: 0,
    sx: 0,
    sy: 0,
    sz: 0
  },
  cilinder: {
    show: false,
    primitive: gl.TRIANGLES,
    color: [0, 0, 255],
    tx: 0,
    ty: 0,
    tz: 0,
    rx: 270,
    ry: 0,
    rz: 0,
    sx: 0,
    sy: 0,
    sz: 0
  },
  cube: {
    show: false,
    primitive: gl.TRIANGLES,
    color: [0, 0, 255],
    tx: 0,
    ty: 0,
    tz: 0,
    rx: 0,
    ry: 0,
    rz: 0,
    sx: 0,
    sy: 0,
    sz: 0
  }
}

// Settings Folder
const settingsFolder = gui.addFolder('Global Settings')
settingsFolder.addColor(state.settings, 'backgroundColor')
settingsFolder.addColor(state.settings, 'xAxisColor')
settingsFolder.addColor(state.settings, 'yAxisColor')
settingsFolder.addColor(state.settings, 'zAxisColor')
settingsFolder.addColor(state.settings, 'gridColor')
settingsFolder.add(state.settings, 'gridSize', 0, 100, 2)

// Camera Folder
const cameraFolder = gui.addFolder('Camera')
cameraFolder.add(state.camera, 'perspective')
cameraFolder.add(state.camera, 'centerX', -360, 360, 0.001)
cameraFolder.add(state.camera, 'centerY', -360, 360, 0.001)
cameraFolder.add(state.camera, 'centerZ', -360, 360, 0.001)
cameraFolder.add(state.camera, 'eyeX', -360, 360, 0.001)
cameraFolder.add(state.camera, 'eyeY', -360, 360, 0.001)
cameraFolder.add(state.camera, 'eyeZ', -360, 360, 0.001)
cameraFolder.add(state.camera, 'upX', -360, 360, 0.001)
cameraFolder.add(state.camera, 'upY', -360, 360, 0.001)
cameraFolder.add(state.camera, 'upZ', -360, 360, 0.001)

cameraFolder.add(state.camera, 'fovy', -180, 180, 0.001)
cameraFolder.add(state.camera, 'aspect', 0, 2 * gl.canvas.width / gl.canvas.height, 0.001)

cameraFolder.add(state.camera, 'left', -100, 100, 0.001)
cameraFolder.add(state.camera, 'right', -100, 100, 0.001)
cameraFolder.add(state.camera, 'bottom', -100, 100, 0.001)
cameraFolder.add(state.camera, 'top', -100, 100, 0.001)

cameraFolder.add(state.camera, 'near', 0, 1000, 0.001)
cameraFolder.add(state.camera, 'far', 0, 1000, 0.001)

// Sphere Folder
const sphereFolder = gui.addFolder('Sphere')
sphereFolder.add(state.sphere, 'show')
sphereFolder.add(state.sphere, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
sphereFolder.addColor(state.sphere, 'color')
sphereFolder.add(state.sphere, 'tx', -1, 1, 0.001)
sphereFolder.add(state.sphere, 'ty', -1, 1, 0.001)
sphereFolder.add(state.sphere, 'tz', -1, 1, 0.001)
sphereFolder.add(state.sphere, 'rx', 0, 360, 0.1)
sphereFolder.add(state.sphere, 'ry', 0, 360, 0.1)
sphereFolder.add(state.sphere, 'rz', 0, 360, 0.1)
sphereFolder.add(state.sphere, 'sx', -1, 1, 0.001)
sphereFolder.add(state.sphere, 'sy', -1, 1, 0.001)
sphereFolder.add(state.sphere, 'sz', -1, 1, 0.001)

// Cone Folder
const coneFolder = gui.addFolder('Cone')
coneFolder.add(state.cone, 'show')
coneFolder.add(state.cone, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
coneFolder.addColor(state.cone, 'color')
coneFolder.add(state.cone, 'tx', -1, 1, 0.001)
coneFolder.add(state.cone, 'ty', -1, 1, 0.001)
coneFolder.add(state.cone, 'tz', -1, 1, 0.001)
coneFolder.add(state.cone, 'rx', 0, 360, 0.1)
coneFolder.add(state.cone, 'ry', 0, 360, 0.1)
coneFolder.add(state.cone, 'rz', 0, 360, 0.1)
coneFolder.add(state.cone, 'sx', -1, 1, 0.001)
coneFolder.add(state.cone, 'sy', -1, 1, 0.001)
coneFolder.add(state.cone, 'sz', -1, 1, 0.001)

// Cilinder Folder
const cilinderFolder = gui.addFolder('Cilinder')
cilinderFolder.add(state.cilinder, 'show')
cilinderFolder.add(state.cilinder, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
cilinderFolder.addColor(state.cilinder, 'color')
cilinderFolder.add(state.cilinder, 'tx', -1, 1, 0.001)
cilinderFolder.add(state.cilinder, 'ty', -1, 1, 0.001)
cilinderFolder.add(state.cilinder, 'tz', -1, 1, 0.001)
cilinderFolder.add(state.cilinder, 'rx', 0, 360, 0.1)
cilinderFolder.add(state.cilinder, 'ry', 0, 360, 0.1)
cilinderFolder.add(state.cilinder, 'rz', 0, 360, 0.1)
cilinderFolder.add(state.cilinder, 'sx', -1, 1, 0.001)
cilinderFolder.add(state.cilinder, 'sy', -1, 1, 0.001)
cilinderFolder.add(state.cilinder, 'sz', -1, 1, 0.001)

// Cube Folder
const cubeFolder = gui.addFolder('Cube')
cubeFolder.add(state.cube, 'show')
cubeFolder.add(state.cube, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
cubeFolder.addColor(state.cube, 'color')
cubeFolder.add(state.cube, 'tx', -1, 1, 0.001)
cubeFolder.add(state.cube, 'ty', -1, 1, 0.001)
cubeFolder.add(state.cube, 'tz', -1, 1, 0.001)
cubeFolder.add(state.cube, 'rx', 0, 360, 0.1)
cubeFolder.add(state.cube, 'ry', 0, 360, 0.1)
cubeFolder.add(state.cube, 'rz', 0, 360, 0.1)
cubeFolder.add(state.cube, 'sx', -1, 1, 0.001)
cubeFolder.add(state.cube, 'sy', -1, 1, 0.001)
cubeFolder.add(state.cube, 'sz', -1, 1, 0.001)

module.exports = {
  state
}
