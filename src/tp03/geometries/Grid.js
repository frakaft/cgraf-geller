const Geometry = require('./Geometry')

class Vector extends Geometry {
  constructor (size) {
    let vertices = []

    for (let i = -size / 2; i <= size / 2; i++) {
      vertices.push(-size / 2)
      vertices.push(0)
      vertices.push(i)
      vertices.push(size / 2)
      vertices.push(0)
      vertices.push(i)
    }

    for (let i = -size / 2; i <= size / 2; i++) {
      vertices.push(i)
      vertices.push(0)
      vertices.push(-size / 2)
      vertices.push(i)
      vertices.push(0)
      vertices.push(size / 2)
    }

    let faces = []

    for (let i = 0; i < (size * 2 + 2) * 2; i++) {
      faces[i] = i
    }

    super(vertices, faces)
  }
}

module.exports = Vector
