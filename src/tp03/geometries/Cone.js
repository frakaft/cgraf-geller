
const Geometry = require('./Geometry')
const RegularConvexPolygonGeometry = require('./RegularConvexPolygonGeometry')

class Cone extends Geometry {
  constructor (edges = 30, height = 1) {
    const circle = new RegularConvexPolygonGeometry(edges)
    let vertices = circle.vertices
    vertices.push(0, 0, height)
    let faces = circle.faces
    for (let i = 0; i < edges; i++) {
      faces.push(i, (i + 1) % edges, edges)
    }
    super(vertices, faces)
  }
}

module.exports = Cone
