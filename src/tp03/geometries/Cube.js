const Geometry = require('./Geometry')

class Cube extends Geometry {
  constructor (size = 1) {
    const vertices = [
      size, size, size,
      size, size, -size,
      size, -size, -size,
      size, -size, size,
      -size, -size, size,
      -size, size, size,
      -size, size, -size,
      -size, -size, -size
    ]
    const faces = [
      0, 1, 3, 0, 5, 3, 0, 5, 1,
      2, 3, 1, 2, 3, 7, 2, 1, 7,
      6, 7, 1, 6, 5, 1, 6, 5, 7,
      4, 3, 7, 4, 3, 5, 4, 5, 7
    ]
    super(vertices, faces)
  }
}

module.exports = Cube
