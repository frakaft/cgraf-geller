const Geometry = require('./Geometry')
const RegularConvexPolygonGeometry = require('./RegularConvexPolygonGeometry')

class Cylinder extends Geometry {
  constructor (edges = 30, height = 1) {
    const circleTop = new RegularConvexPolygonGeometry(edges, height)
    const circleBottom = new RegularConvexPolygonGeometry(edges)

    let vertices = circleTop.vertices
    vertices.push(...circleBottom.vertices)

    let faces = circleTop.faces
    for (let i = 0; i < circleBottom.faces.length; i++) {
      circleBottom.faces[i] = circleBottom.faces[i] + edges
    }
    faces.push(...circleBottom.faces)

    for (let i = 0; i < edges; i++) {
      faces.push(i, (i + 1) % edges, edges + i)
      faces.push(edges + i, edges + ((i + 1) % edges), (i + 1) % edges)
    }
    super(vertices, faces)
  }
}

module.exports = Cylinder
