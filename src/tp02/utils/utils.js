/**
 * Resize canvas if needed
 */
function resizeCanvas (canvas) {
  var realToCSSPixels = window.devicePixelRatio
  // Lookup the size the browser is displaying the canvas in CSS pixels
  // and compute a size needed to make our drawingbuffer match it in
  // device pixels.
  var displayWidth = Math.floor(canvas.clientWidth * realToCSSPixels)
  var displayHeight = Math.floor(canvas.clientHeight * realToCSSPixels)

  // Check if the canvas is not the same size.
  if (canvas.width !== displayWidth ||
      canvas.height !== displayHeight) {
    // Make the canvas the same size
    canvas.width = displayWidth
    canvas.height = displayHeight
  }
}

/**
 * rgb in 255 format to rgba normaliced format
 */
function rgba (rgb) {
  return [rgb[0] / 255, rgb[1] / 255, rgb[2] / 255, 1]
}

function radToDeg (r) {
  return r * 180 / Math.PI
}

function degToRad (d) {
  return (d % 360) * Math.PI / 180
}

module.exports = {
  resizeCanvas,
  rgba,
  radToDeg,
  degToRad
}
