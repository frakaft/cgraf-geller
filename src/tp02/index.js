const dat = require('dat.gui')
const Mesh = require('./Mesh')
const WebGLRenderer = require('./WebGLRenderer')
const RegularConvexPolygonGeometry = require('./RegularConvexPolygonGeometry')
const Geometry = require('./Geometry')
const Scene = require('./Scene')
const { rgba, degToRad } = require('./utils/utils')

const state = {
  figure1: {
    edges: 3,
    color: [255, 0, 0],
    tx: -0.1,
    ty: 0.1,
    tz: 0,
    rx: 0,
    ry: 0,
    rz: 0,
    sx: 1,
    sy: 1,
    sz: 1
  },
  figure2: {
    edges: 3,
    color: [255, 255, 0],
    tx: 0,
    ty: 0,
    tz: 0,
    rx: 0,
    ry: 0,
    rz: 0,
    sx: 1,
    sy: 1,
    sz: 1
  },
  figure3: {
    edges: 3,
    color: [255, 0, 255],
    tx: 0.1,
    ty: -0.1,
    tz: 0,
    rx: 0,
    ry: 0,
    rz: 0,
    sx: 1,
    sy: 1,
    sz: 1
  },
  backgroundColor: [51, 51, 51],
  xAxisColor: [255, 0, 0],
  yAxisColor: [0, 255, 0]
}

const gui = new dat.GUI()

const f0 = gui.addFolder('Global Settings')
f0.addColor(state, 'backgroundColor')
f0.addColor(state, 'xAxisColor')
f0.addColor(state, 'yAxisColor')

const canvas = document.getElementById('c')
const gl = canvas.getContext('webgl')

const f1 = gui.addFolder('Figure 1')
f1.add(state.figure1, 'edges', 3, 100, 1)
f1.addColor(state.figure1, 'color')
f1.add(state.figure1, 'tx', -2, 2, 0.001)
f1.add(state.figure1, 'ty', -2, 2, 0.001)
f1.add(state.figure1, 'tz', -2, 2, 0.001)
f1.add(state.figure1, 'rx', 0, 360, 0.1)
f1.add(state.figure1, 'ry', 0, 360, 0.1)
f1.add(state.figure1, 'rz', 0, 360, 0.1)
f1.add(state.figure1, 'sx', -2, 2, 0.001)
f1.add(state.figure1, 'sy', -2, 2, 0.001)
f1.add(state.figure1, 'sz', -2, 2, 0.001)

const f2 = gui.addFolder('Figure 2')
f2.add(state.figure2, 'edges', 3, 100, 1)
f2.addColor(state.figure2, 'color')
f2.add(state.figure2, 'tx', -2, 2, 0.001)
f2.add(state.figure2, 'ty', -2, 2, 0.001)
f2.add(state.figure2, 'tz', -2, 2, 0.001)
f2.add(state.figure2, 'rx', 0, 360, 0.1)
f2.add(state.figure2, 'ry', 0, 360, 0.1)
f2.add(state.figure2, 'rz', 0, 360, 0.1)
f2.add(state.figure2, 'sx', -2, 2, 0.001)
f2.add(state.figure2, 'sy', -2, 2, 0.001)
f2.add(state.figure2, 'sz', -2, 2, 0.001)

const f3 = gui.addFolder('Figure 3')
f3.add(state.figure3, 'edges', 3, 100, 1)
f3.addColor(state.figure3, 'color')
f3.add(state.figure3, 'tx', -2, 2, 0.001)
f3.add(state.figure3, 'ty', -2, 2, 0.001)
f3.add(state.figure3, 'tz', -2, 2, 0.001)
f3.add(state.figure3, 'rx', 0, 360, 0.1)
f3.add(state.figure3, 'ry', 0, 360, 0.1)
f3.add(state.figure3, 'rz', 0, 360, 0.1)
f3.add(state.figure3, 'sx', -2, 2, 0.001)
f3.add(state.figure3, 'sy', -2, 2, 0.001)
f3.add(state.figure3, 'sz', -2, 2, 0.001)

function render (time) {
  // Figure 1
  const polygon1 = new RegularConvexPolygonGeometry(state.figure1.edges) // Edges
  const material1 = new Float32Array(rgba(state.figure1.color)) // Figure color
  const mesh1 = new Mesh(polygon1, material1, gl.TRIANGLES)
  mesh1.tx = state.figure1.tx
  mesh1.ty = state.figure1.ty
  mesh1.tz = state.figure1.tz
  mesh1.rx = degToRad(state.figure1.rx)
  mesh1.ry = degToRad(state.figure1.ry)
  mesh1.rz = degToRad(state.figure1.rz)
  mesh1.sx = state.figure1.sx
  mesh1.sy = state.figure1.sy
  mesh1.sz = state.figure1.sz

  // Figure 2
  const polygon2 = new RegularConvexPolygonGeometry(state.figure2.edges) // Edges
  const material2 = new Float32Array(rgba(state.figure2.color)) // Figure color
  const mesh2 = new Mesh(polygon2, material2, gl.TRIANGLES)
  mesh2.tx = state.figure2.tx
  mesh2.ty = state.figure2.ty
  mesh2.tz = state.figure2.tz
  mesh2.rx = degToRad(state.figure2.rx)
  mesh2.ry = degToRad(state.figure2.ry)
  mesh2.rz = degToRad(state.figure2.rz)
  mesh2.sx = state.figure2.sx
  mesh2.sy = state.figure2.sy
  mesh2.sz = state.figure2.sz

  // Figure 3
  const polygon3 = new RegularConvexPolygonGeometry(state.figure3.edges) // Edges
  const material3 = new Float32Array(rgba(state.figure3.color)) // Figure color
  const mesh3 = new Mesh(polygon3, material3, gl.TRIANGLES)
  mesh3.tx = state.figure3.tx
  mesh3.ty = state.figure3.ty
  mesh3.tz = state.figure3.tz
  mesh3.rx = degToRad(state.figure3.rx)
  mesh3.ry = degToRad(state.figure3.ry)
  mesh3.rz = degToRad(state.figure3.rz)
  mesh3.sx = state.figure3.sx
  mesh3.sy = state.figure3.sy
  mesh3.sz = state.figure3.sz

  // Axis x
  const xAxis = new Geometry([
    0.0, 0.0, 0.0,
    1.0, 0.0, 0.0
  ], [0, 1])
  const xAxisMaterial = new Float32Array(rgba(state.xAxisColor)) // Figure color
  const xAxisMesh = new Mesh(xAxis, xAxisMaterial, gl.LINES)

  // Axis y
  const yAxis = new Geometry([
    0.0, 0.0, 0.0,
    0.0, 1.0, 0.0
  ], [0, 1])
  const yAxisMaterial = new Float32Array(rgba(state.yAxisColor)) // Figure color
  const yAxisMesh = new Mesh(yAxis, yAxisMaterial, gl.LINES)

  // Scene
  const scene = new Scene(rgba(state.backgroundColor)) // ClearColor

  scene.addMesh(mesh1)
  scene.addMesh(mesh2)
  scene.addMesh(mesh3)
  scene.addMesh(xAxisMesh)
  scene.addMesh(yAxisMesh)

  const webGLInstance = new WebGLRenderer(canvas)
  webGLInstance.render(scene, NaN) // (scene, camera)

  window.requestAnimationFrame(render)
}

window.requestAnimationFrame(render)
