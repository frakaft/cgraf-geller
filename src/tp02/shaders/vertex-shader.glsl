attribute vec4 aPosition;

uniform vec4 uColor;

uniform mat4 uModelMatrix;

varying vec4 vColor;

void main() {
  gl_Position = uModelMatrix * aPosition;
  vColor = uColor;
}
