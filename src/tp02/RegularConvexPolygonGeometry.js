const Geometry = require('./Geometry')

class RegularConvexPolygonGeometry extends Geometry {
  constructor (edges = 3) {
    let vertices = []
    for (let i = 0; i < edges; i++) {
      vertices.push(Math.cos(((360 * Math.PI / 180) / edges) * i))
      vertices.push(Math.sin(((360 * Math.PI / 180) / edges) * i))
      vertices.push(0)
    }

    let faces = []
    for (let i = 0; i < edges - 2; i++) {
      faces.push(0)
      faces.push(i + 1)
      faces.push(i + 2)
    }

    super(vertices, faces)
  }
}

module.exports = RegularConvexPolygonGeometry
