const { resizeCanvas } = require('./utils/utils')
const vsSource = require('./shaders/vertex-shader.glsl')
const fsSource = require('./shaders/fragment-shader.glsl')

class WebGLRenderer {
  constructor (canvas) {
    const gl = canvas.getContext('webgl')

    const vertexShader = gl.createShader(gl.VERTEX_SHADER)
    gl.shaderSource(vertexShader, vsSource)
    gl.compileShader(vertexShader)

    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER)
    gl.shaderSource(fragmentShader, fsSource)
    gl.compileShader(fragmentShader)

    const program = gl.createProgram()
    gl.attachShader(program, vertexShader)
    gl.attachShader(program, fragmentShader)

    gl.linkProgram(program)
    gl.useProgram(program)

    resizeCanvas(gl.canvas)

    this.gl = gl
    this.program = program
  }

  render (scene, camera) {
    const gl = this.gl
    const program = this.program

    // Specify the color for clearing <canvas>
    gl.clearColor(scene.clearColor.r, scene.clearColor.g, scene.clearColor.b, scene.clearColor.a)

    // Clear <canvas>
    gl.clear(gl.COLOR_BUFFER_BIT)

    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight)

    // Cache attribute/uniform location
    const aPosition = gl.getAttribLocation(program, 'aPosition')
    gl.enableVertexAttribArray(aPosition)
    const uColor = gl.getUniformLocation(program, 'uColor')
    const uModelMatrix = gl.getUniformLocation(program, 'uModelMatrix')

    for (let i = 0; i < scene.meshes.length; i++) {
      const mesh = scene.meshes[i]

      // Create Vertex Buffer Object
      if (!mesh.vbo) {
        mesh.vbo = gl.createBuffer()
        gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vbo)
        gl.bufferData(gl.ARRAY_BUFFER, mesh.VBOData, gl.STATIC_DRAW)
        gl.bindBuffer(gl.ARRAY_BUFFER, null)
      }

      gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vbo)

      // Bind Buffer to a shader attribute
      gl.vertexAttribPointer(
        aPosition, 3, gl.FLOAT, false, mesh.VBOData.BYTES_PER_ELEMENT * 0, mesh.VBOData.BYTES_PER_ELEMENT * 0
      )

      // Clean up
      gl.bindBuffer(gl.ARRAY_BUFFER, null)

      // Create Index Buffer Object
      if (!mesh.ibo) {
        mesh.ibo = gl.createBuffer()
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ibo)
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, mesh.IBOData, gl.STATIC_DRAW)
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)
      }
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ibo)

      gl.uniform4fv(uColor, mesh.material)
      gl.uniformMatrix4fv(uModelMatrix, false, mesh.getModelMatrix())

      // Draw Triangle
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ibo)
      gl.drawElements(mesh.primitive, mesh.IBOData.length, gl.UNSIGNED_SHORT, 0)
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)

      // Free buffered memory (only if the mesh changes)
      gl.deleteBuffer(mesh.vbo)
      gl.deleteBuffer(mesh.ibo)
    }
  }
}

module.exports = WebGLRenderer
