class Material {
  constructor (color, diffuse = undefined, specular = undefined, shininess = undefined, obscure = true, map = undefined, useTexture = false) {
    this.color = color
    this.diffuse = diffuse
    this.specular = specular
    this.shininess = shininess
    this.obscure = obscure
    this.map = map
    this.useTexture = useTexture
  }
  getDiffuse () {
    return [(this.color[0] / 255) * this.diffuse, (this.color[1] / 255) * this.diffuse, (this.color[2] / 255) * this.diffuse]
  }
  getSpecular () {
    return [(this.color[0] / 255) * this.specular, (this.color[1] / 255) * this.specular, (this.color[2] / 255) * this.specular]
  }
}

module.exports = Material
