const vsSourceLightTexture = require('./shaders/vertex-shader-light-texture.glsl')
const fsSourceLightTexture = require('./shaders/fragment-shader-light-texture.glsl')
const vsSource = require('./shaders/vertex-shader.glsl')
const fsSource = require('./shaders/fragment-shader.glsl')
const twgl = require('twgl.js')

class WebGLRenderer {
  constructor (canvas) {
    this.gl = canvas.getContext('webgl')
    this.programObscure = twgl.createProgramInfo(this.gl, [vsSource, fsSource])
    this.programLightTexture = twgl.createProgramInfo(this.gl, [vsSourceLightTexture, fsSourceLightTexture])

    // Picking Object
    this.framebuffer = this.createFramebufferInfo(this.gl)
    const contextReference = this
    this.gl.canvas.addEventListener('click', function (evt) {
      contextReference.pickingCoord = {
        x: evt.offsetX,
        y: contextReference.gl.canvas.height - evt.offsetY
      }
    })
  }
  render (scene, camera) {
    const gl = this.gl
    gl.clearColor(scene.clearColor[0] / 255, scene.clearColor[1] / 255, scene.clearColor[2] / 255, 1)
    gl.enable(gl.DEPTH_TEST)
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
    twgl.resizeCanvasToDisplaySize(gl.canvas)
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight)

    for (let i = 0; i < scene.meshes.length; i++) {
      const mesh = scene.meshes[i]
      this.draw(gl, mesh, camera, scene)
      if (mesh.picked) {
        for (let i = 0; i < mesh.cosmetics.length; i++) {
          this.draw(gl, mesh.cosmetics[i], camera, scene)
        }
      }
    }

    // Picking Object
    if (this.pickingCoord != null) {
      this.picked(gl, scene, camera)
    }
  }
  draw (gl, mesh, camera, scene) {
    if (mesh.active) {
      if (!mesh.texture) {
        mesh.texture = mesh.material.obscure ? undefined : twgl.createTexture(gl, {
          src: mesh.material.map
        })
      }
      mesh.program = mesh.material.obscure ? this.programObscure : this.programLightTexture
      mesh.data = mesh.material.obscure ? {
        aPosition: mesh.VBOData,
        indices: mesh.IBOData
      } : {
        aPosition: mesh.VBOData,
        indices: mesh.IBOData,
        aNormal: mesh.normals,
        aTexCoords: mesh.geometry.st
      }
      mesh.uniforms = mesh.material.obscure ? {
        uColor: [mesh.material.color[0] / 255, mesh.material.color[1] / 255, mesh.material.color[2] / 255, 1],
        uModelMatrix: mesh.getModelMatrix(),
        uViewMatrix: camera.getViewMatrix(),
        uProjectionMatrix: camera.getProjectionMatrix()
      } : {
        uModelMatrix: mesh.getModelMatrix(),
        uNormalMatrix: mesh.getNormalMatrix(),
        uViewMatrix: camera.getViewMatrix(),
        uProjectionMatrix: camera.getProjectionMatrix(),
        uViewPos: camera.getViewPos(),

        'material.texture': mesh.texture,
        'material.useTexture': mesh.material.useTexture,

        'material.diffuse': mesh.material.getDiffuse(),
        'material.specular': mesh.material.getSpecular(),
        'material.shininess': mesh.material.shininess,
        'ambientLight.active': scene.ambientLight.active,
        'ambientLight.color': scene.ambientLight.getAmbient(),

        'dirLights[0].active': scene.dirLights[0].active,
        'dirLights[0].direction': scene.dirLights[0].getDirection(),
        'dirLights[0].ambient': scene.dirLights[0].getAmbient(),
        'dirLights[0].diffuse': scene.dirLights[0].getDiffuse(),
        'dirLights[0].specular': scene.dirLights[0].getSpecular(),

        'pointLights[0].active': scene.pointLights[0].active,
        'pointLights[0].position': scene.pointLights[0].getPosition(),
        'pointLights[0].ambient': scene.pointLights[0].getAmbient(),
        'pointLights[0].diffuse': scene.pointLights[0].getDiffuse(),
        'pointLights[0].specular': scene.pointLights[0].getSpecular(),
        'pointLights[0].constant': scene.pointLights[0].constant,
        'pointLights[0].linear': scene.pointLights[0].linear,
        'pointLights[0].quadratic': scene.pointLights[0].quadratic,

        'pointLights[1].active': scene.pointLights[1].active,
        'pointLights[1].position': scene.pointLights[1].getPosition(),
        'pointLights[1].ambient': scene.pointLights[1].getAmbient(),
        'pointLights[1].diffuse': scene.pointLights[1].getDiffuse(),
        'pointLights[1].specular': scene.pointLights[1].getSpecular(),
        'pointLights[1].constant': scene.pointLights[1].constant,
        'pointLights[1].linear': scene.pointLights[1].linear,
        'pointLights[1].quadratic': scene.pointLights[1].quadratic,

        'pointLights[2].active': scene.pointLights[2].active,
        'pointLights[2].position': scene.pointLights[2].getPosition(),
        'pointLights[2].ambient': scene.pointLights[2].getAmbient(),
        'pointLights[2].diffuse': scene.pointLights[2].getDiffuse(),
        'pointLights[2].specular': scene.pointLights[2].getSpecular(),
        'pointLights[2].constant': scene.pointLights[2].constant,
        'pointLights[2].linear': scene.pointLights[2].linear,
        'pointLights[2].quadratic': scene.pointLights[2].quadratic,

        'spotLights[0].active': scene.spotLights[0].active,
        'spotLights[0].position': scene.spotLights[0].getPosition(),
        'spotLights[0].direction': scene.spotLights[0].getDirection(),
        'spotLights[0].cutOff': scene.spotLights[0].getCutOff(),
        'spotLights[0].outerCutOff': scene.spotLights[0].getOuterCutOff(),
        'spotLights[0].ambient': scene.spotLights[0].getAmbient(),
        'spotLights[0].diffuse': scene.spotLights[0].getDiffuse(),
        'spotLights[0].specular': scene.spotLights[0].getSpecular(),
        'spotLights[0].constant': scene.spotLights[0].constant,
        'spotLights[0].linear': scene.spotLights[0].linear,
        'spotLights[0].quadratic': scene.spotLights[0].quadratic
      }
      const bufferInfo = twgl.createBufferInfoFromArrays(this.gl, mesh.data)
      gl.useProgram(mesh.program.program)
      twgl.setBuffersAndAttributes(gl, mesh.program, bufferInfo)
      twgl.setUniforms(mesh.program, mesh.uniforms)
      twgl.drawBufferInfo(gl, bufferInfo, mesh.primitive)
    }
  }
  drawPicking (gl, mesh, camera, index) {
    if (mesh.active) {
      const data = {
        aPosition: mesh.VBOData,
        indices: mesh.IBOData
      }
      const uniforms = {
        uColor: [index, 0, 0, 0],
        uModelMatrix: mesh.getModelMatrix(),
        uViewMatrix: camera.getViewMatrix(),
        uProjectionMatrix: camera.getProjectionMatrix()
      }
      const bufferInfo = twgl.createBufferInfoFromArrays(gl, data)
      gl.useProgram(this.programObscure.program)
      twgl.setBuffersAndAttributes(gl, this.programObscure, bufferInfo)
      twgl.setUniforms(this.programObscure, uniforms)
      twgl.drawBufferInfo(gl, bufferInfo, mesh.primitive)
    }
  }
  createFramebufferInfo (gl) {
    const attachments = [
      {
        format: gl.RGBA,
        type: gl.UNSIGNED_BYTE,
        min: gl.LINEAR,
        mag: gl.LINEAR,
        wrap: gl.CLAMP_TO_EDGE
      },
      {
        format: gl.DEPTH_COMPONENT16
      }
    ]

    twgl.resizeCanvasToDisplaySize(gl.canvas)
    const framebuffer = twgl.createFramebufferInfo(gl, attachments, gl.drawingBufferWidth, gl.drawingBufferHeight)
    gl.bindFramebuffer(gl.FRAMEBUFFER, null)

    return framebuffer
  }
  picked (gl, scene, camera) {
    let readout = new Uint8Array(1 * 1 * 4)
    gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.framebuffer.framebuffer)
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
    for (let i = 0; i < scene.meshes.length; i++) {
      const mesh = scene.meshes[i]
      this.drawPicking(gl, mesh, camera, i / 255)
    }

    gl.readPixels(
      this.pickingCoord.x,
      this.pickingCoord.y,
      1,
      1,
      gl.RGBA,
      gl.UNSIGNED_BYTE,
      readout
    )

    gl.bindFramebuffer(gl.FRAMEBUFFER, null)

    this.printPickedMesh(readout, scene.meshes)
    this.pickingCoord = null
  }
  printPickedMesh (readout, meshes) {
    for (let i = 0; i < meshes.length; i++) {
      const mesh = meshes[i]
      if (i === readout[0]) {
        console.log('ID: ', i)
        console.log(mesh.geometry)
        mesh.picked = !mesh.picked
      }
    }
  }
}
module.exports = WebGLRenderer
