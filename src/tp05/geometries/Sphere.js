const Geometry = require('./Geometry')

class Sphere extends Geometry {
  constructor (radius = 1, latitudeBands = 30, longitudeBands = 30) {
    let vertices = []
    let normals = []
    let texture = []
    for (let latNumber = 0; latNumber <= latitudeBands; latNumber++) {
      const theta = latNumber * Math.PI / latitudeBands
      const sinTheta = Math.sin(theta)
      const cosTheta = Math.cos(theta)

      for (let longNumber = 0; longNumber <= longitudeBands; longNumber++) {
        const phi = longNumber * 2 * Math.PI / longitudeBands
        const sinPhi = Math.sin(phi)
        const cosPhi = Math.cos(phi)

        const x = cosPhi * sinTheta
        const y = cosTheta
        const z = sinPhi * sinTheta

        let u = 1 - (longNumber / longitudeBands)
        let v = 1 - (latNumber / latitudeBands)

        texture.push(u)
        texture.push(v)

        normals.push(x)
        normals.push(y)
        normals.push(z)

        vertices.push(radius * x)
        vertices.push(radius * y)
        vertices.push(radius * z)
      }
    }

    let faces = []
    for (let latNumber = 0; latNumber < latitudeBands; latNumber++) {
      for (let longNumber = 0; longNumber < longitudeBands; longNumber++) {
        const first = (latNumber * (longitudeBands + 1)) + longNumber
        const second = first + longitudeBands + 1
        faces.push(first)
        faces.push(second)
        faces.push(first + 1)

        faces.push(second)
        faces.push(second + 1)
        faces.push(first + 1)
      }
    }
    super(vertices, faces, normals, texture)
  }
}

module.exports = Sphere
