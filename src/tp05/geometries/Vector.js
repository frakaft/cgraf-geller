const Geometry = require('./Geometry')

class Vector extends Geometry {
  constructor (points) {
    let vertices = points
    let faces = [0, 1]
    super(vertices, faces)
  }
}

module.exports = Vector
