const Geometry = require('./Geometry')

class SimpleCube extends Geometry {
  constructor (size = 1) {
    const vertices = [
      size, size, size,
      size, size, -size,
      size, -size, -size,
      size, -size, size,
      -size, -size, size,
      -size, size, size,
      -size, size, -size,
      -size, -size, -size
    ]
    const faces = [
      0, 1, 1, 2, 2, 3, 3, 0, 5, 6, 5, 0, 6, 1, 5, 4, 4, 3, 4, 7, 6, 7, 7, 2
    ]
    super(vertices, faces)
  }
}

module.exports = SimpleCube
