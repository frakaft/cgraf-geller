const { normalize, addition, subtraction, cross } = require('../utils/vec3')

class Geometry {
  constructor (vertices, faces, normals, st) {
    this.vertices = vertices
    this.faces = faces
    this.normals = normals
    this.st = st
  }
  getNormals () {
    if (!this.normals) {
      this.setNormals()
    }
    return this.normals
  }
  setNormals () {
    this.normals = []
    for (let i = 0; i < this.vertices.length; i++) {
      this.normals.push(0)
    }

    for (let i = 0; i < this.faces.length; i += 3) {
      const ia = this.faces[i]
      const ib = this.faces[i + 1]
      const ic = this.faces[i + 2]

      const via = this.vertices.slice(ia * 3, ia * 3 + 3)
      const vib = this.vertices.slice(ib * 3, ib * 3 + 3)
      const vic = this.vertices.slice(ic * 3, ic * 3 + 3)

      const e1 = subtraction(via, vib)
      const e2 = subtraction(vic, vib)
      const no = cross(e2, e1)

      const viat = addition(this.normals.slice(ia * 3, ia * 3 + 3), no)
      this.normals[ia * 3] = viat[0]
      this.normals[ia * 3 + 1] = viat[1]
      this.normals[ia * 3 + 2] = viat[2]
      const vibt = addition(this.normals.slice(ib * 3, ib * 3 + 3), no)
      this.normals[ib * 3] = vibt[0]
      this.normals[ib * 3 + 1] = vibt[1]
      this.normals[ib * 3 + 2] = vibt[2]
      const vict = addition(this.normals.slice(ic * 3, ic * 3 + 3), no)
      this.normals[ic * 3] = vict[0]
      this.normals[ic * 3 + 1] = vict[1]
      this.normals[ic * 3 + 2] = vict[2]
    }

    for (let i = 0; i < this.vertices.length / 3; i++) {
      const normalized = normalize(this.normals.slice(i * 3, i * 3 + 3))
      this.normals[i * 3] = normalized[0]
      this.normals[i * 3 + 1] = normalized[1]
      this.normals[i * 3 + 2] = normalized[2]
    }
  }
}

module.exports = Geometry
