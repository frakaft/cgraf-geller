// Fragment Shader
precision mediump float;

struct Material {
  // sampler2D diffuse;
  // sampler2D specular;
  sampler2D texture;
  bool useTexture;

  vec3 diffuse;
  vec3 specular;
  float shininess;
};

struct AmbientLight {
  bool active;
  vec3 color;
  // vec3 ambient;
  // vec3 diffuse;
  // vec3 specular;
};

struct DirLight {
  bool active;
  vec3 direction;

  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
};

struct PointLight {
  bool active;
  vec3 position;

  float constant;
  float linear;
  float quadratic;

  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
};

struct SpotLight {
  bool active;
  vec3 position;
  vec3 direction;
  float cutOff;
  float outerCutOff;

  float constant;
  float linear;
  float quadratic;

  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
};

#define NR_DIR_LIGHTS 1
#define NR_POINT_LIGHTS 3
#define NR_SPOT_LIGHTS 1

varying vec3 vFragPos;
varying vec3 vNormal;
varying vec2 vTexCoords;

uniform vec3 uViewPos;
uniform AmbientLight ambientLight;
uniform DirLight dirLights[NR_DIR_LIGHTS];
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform SpotLight spotLights[NR_SPOT_LIGHTS];
uniform Material material;

// Function prototypes
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
  // Properties
  vec3 norm = normalize(vNormal);
  // Ver normales
  // gl_FragColor = vec4(norm, 1.0);
  // return;
  vec3 viewDir = normalize(uViewPos - vFragPos);

  vec3 result = vec3(0.0);

  // --------------------------------
  // gl_FragColor = texture2D(uTexture, vTexCoords);
  // test vTexCoords (red->green shading. If not: bad texture coordinates)
  // gl_FragColor = vec4(vTexCoords,0,1);
  // return;
  // --------------------------------
  if(ambientLight.active)
    result = ambientLight.color * material.diffuse;
  // == ======================================
  // Our lighting is set up in 3 phases: directional, point lights and an optional flashlight
  // For each phase, a calculate function is defined that calculates the corresponding color
  // per lamp. In the main() function we take all the calculated colors and sum them up for
  // this fragment's final color.
  // == ======================================
  // Phase 1: Directional lighting
  for(int i = 0; i < NR_DIR_LIGHTS; i++) {
    if(dirLights[i].active == true) {
      result += CalcDirLight(dirLights[i], norm, viewDir);
    }
  }
  // Ver color dependiendo posicion de la luz
  // gl_FragColor = vec4(dirLight.direction, 1.0);
  // return;
  // Phase 2: Point lights
  for(int i = 0; i < NR_POINT_LIGHTS; i++) {
      if (pointLights[i].active == true) {
        result += CalcPointLight(pointLights[i], norm, vFragPos, viewDir);
      }
  }
  // Phase 3: Spot light
  for(int i = 0; i < NR_SPOT_LIGHTS; i++) {
    if(spotLights[i].active == true) {
      result += CalcSpotLight(spotLights[i], norm, vFragPos, viewDir);
    }
  }

  if(material.useTexture){
    gl_FragColor = texture2D(material.texture, vTexCoords) * vec4(result, 1.0);
  } else {
    gl_FragColor = vec4(result, 1.0);
  }
  // gl_FragColor = vec4((norm + vec3(0.5)) * 0.5, 1.0);
}

// Calculates the color when using a directional light.
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
   vec3 lightDir = normalize(-light.direction); // Profe
  //vec3 lightDir = normalize(light.direction - vFragPos); // Mio
  // Diffuse shading
  float diff = max(dot(normal, lightDir ), 0.0);
  // Specular shading
  vec3 reflectDir = reflect(-lightDir, normal);
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
  // Combine results
  vec3 ambient, diffuse, specular;
  if(material.useTexture) {
    ambient = light.ambient * vec3(texture2D(material.texture, vTexCoords));
    diffuse = light.diffuse * diff * vec3(texture2D(material.texture, vTexCoords));
    specular = light.specular * spec * vec3(texture2D(material.texture, vTexCoords));
  } else {
    ambient = light.ambient * material.diffuse;
    diffuse = light.diffuse * diff * material.diffuse;
    specular = light.specular * spec * material.specular;
  }

  return (ambient + diffuse + specular);
}

// Calculates the color when using a point light.
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
  vec3 lightDir = normalize(light.position - fragPos);
  // Diffuse shading
  float diff = max(dot(normal, lightDir), 0.0);
  // Specular shading
  vec3 reflectDir = reflect(-lightDir, normal);
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
  // Attenuation
  float distance = length(light.position - fragPos);
  float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
  // Combine results
  vec3 ambient, diffuse, specular;
  if(material.useTexture) {
    ambient = light.ambient * vec3(texture2D(material.texture, vTexCoords));
    diffuse = light.diffuse * diff * vec3(texture2D(material.texture, vTexCoords));
    specular = light.specular * spec * vec3(texture2D(material.texture, vTexCoords));
  } else {
    ambient = light.ambient * material.diffuse;
    diffuse = light.diffuse * diff * material.diffuse;
    specular = light.specular * spec * material.specular;
  }
  ambient *= attenuation;
  diffuse *= attenuation;
  specular *= attenuation;
  return (ambient + diffuse + specular);
}

// Calculates the color when using a spot light.
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
  vec3 lightDir = normalize(light.position - fragPos);
  // Diffuse shading
  float diff = max(dot(normal, lightDir), 0.0);
  // Specular shading
  vec3 reflectDir = reflect(-lightDir, normal);
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
  // Attenuation
  float distance = length(light.position - fragPos);
  float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
  // Spotlight intensity
  float theta = dot(lightDir, normalize(-light.direction));
  float epsilon = light.cutOff - light.outerCutOff;
  float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
  // Combine results
  vec3 ambient, diffuse, specular;
  if(material.useTexture) {
    ambient = light.ambient * vec3(texture2D(material.texture, vTexCoords));
    diffuse = light.diffuse * diff * vec3(texture2D(material.texture, vTexCoords));
    specular = light.specular * spec * vec3(texture2D(material.texture, vTexCoords));
  } else {
    ambient = light.ambient * material.diffuse;
    diffuse = light.diffuse * diff * material.diffuse;
    specular = light.specular * spec * material.specular;
  }
  ambient *= attenuation * intensity;
  diffuse *= attenuation * intensity;
  specular *= attenuation * intensity;
  return (ambient + diffuse + specular);
}
