class Scene {
  constructor (clearColor, ambientLight) {
    this.clearColor = clearColor
    this.meshes = []
    this.ambientLight = ambientLight
    this.dirLights = []
    this.pointLights = []
    this.spotLights = []
  }
  addMesh (mesh) {
    this.meshes.push(mesh)
  }
  addDirLight (dirLight) {
    this.dirLights.push(dirLight)
  }
  addPointLight (pointLight) {
    this.pointLights.push(pointLight)
  }
  addSpotLight (spotLight) {
    this.spotLights.push(spotLight)
  }
}

module.exports = Scene
