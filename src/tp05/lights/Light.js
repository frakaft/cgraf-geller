class Light {
  constructor (color, active = true) {
    this.color = color
    this.active = active
    this.cosmetics = []
  }
  addCosmetic (mesh) {
    this.cosmetics.push(mesh)
  }
}

module.exports = Light
