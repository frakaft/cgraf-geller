const Light = require('./Light')

class DirectionalLight extends Light {
  constructor (color, ambient, diffuse, specular, dx, dy, dz, active) {
    super(color, active)
    this.ambient = ambient
    this.diffuse = diffuse
    this.specular = specular
    this.dx = dx
    this.dy = dy
    this.dz = dz
  }
  getAmbient () {
    return [(this.color[0] / 255) * this.ambient, (this.color[1] / 255) * this.ambient, (this.color[2] / 255) * this.ambient]
  }
  getDiffuse () {
    return [(this.color[0] / 255) * this.diffuse, (this.color[1] / 255) * this.diffuse, (this.color[2] / 255) * this.diffuse]
  }
  getSpecular () {
    return [(this.color[0] / 255) * this.specular, (this.color[1] / 255) * this.specular, (this.color[2] / 255) * this.specular]
  }
  getDirection () {
    return [this.dx, this.dy, this.dz]
  }
}

module.exports = DirectionalLight
