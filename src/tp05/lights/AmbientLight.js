const Light = require('./Light')

class AmbientLight extends Light {
  constructor (color, ambient, active = true) {
    super(color, active)
    this.ambient = ambient
  }
  getAmbient () {
    return [(this.color[0] / 255) * this.ambient, (this.color[1] / 255) * this.ambient, (this.color[2] / 255) * this.ambient]
  }
}

module.exports = AmbientLight
