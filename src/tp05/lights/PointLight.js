const Light = require('./Light')

class PointLight extends Light {
  constructor (color, ambient, diffuse, specular, px, py, pz, active = true, constant = 1.0, linear = 0.09, quadratic = 0.032) {
    super(color, active)
    this.ambient = ambient
    this.diffuse = diffuse
    this.specular = specular
    this.px = px
    this.py = py
    this.pz = pz
    this.constant = constant
    this.linear = linear
    this.quadratic = quadratic
  }
  getAmbient () {
    return [(this.color[0] / 255) * this.ambient, (this.color[1] / 255) * this.ambient, (this.color[2] / 255) * this.ambient]
  }
  getDiffuse () {
    return [(this.color[0] / 255) * this.diffuse, (this.color[1] / 255) * this.diffuse, (this.color[2] / 255) * this.diffuse]
  }
  getSpecular () {
    return [(this.color[0] / 255) * this.specular, (this.color[1] / 255) * this.specular, (this.color[2] / 255) * this.specular]
  }
  getPosition () {
    return [this.px, this.py, this.pz]
  }
}

module.exports = PointLight
