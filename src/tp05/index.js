const Mesh = require('./mesh/Mesh')
const WebGLRenderer = require('./WebGLRenderer')
const Scene = require('./Scene')
const PerspectiveCamera = require('./camera/PerspectiveCamera')
const OrthographicCamera = require('./camera/OrthographicCamera')
const Grid = require('./geometries/Grid')
const Sphere = require('./geometries/Sphere')
// const Cilinder = require('./geometries/Cilinder')
// const Cube = require('./geometries/Cube')
const SimpleCube = require('./geometries/SimpleCube')
const Material = require('./mesh/Material')
const AmbientLight = require('./lights/AmbientLight')
const DirectionalLight = require('./lights/DirectionalLight')
const PointLight = require('./lights/PointLight')
const SpotLight = require('./lights/SpotLight')
const { AxisX, AxisY, AxisZ } = require('./geometries/Axis')
const { degToRad, disableRightClickContextMenu, unormalize } = require('./utils/utils')
const { def } = require('./config/default')
const dat = require('dat.gui')

const canvas = document.getElementById('c')
const gl = canvas.getContext('webgl')
disableRightClickContextMenu(canvas) // for mouse events

// Camera setup
let camera
const perspectiveCamera = new PerspectiveCamera(degToRad(def.camera.fovy), def.camera.aspect, def.camera.near, def.camera.far)
const orthographicCamera = new OrthographicCamera(def.camera.left, def.camera.right, def.camera.bottom, def.camera.top, def.camera.near, def.camera.far)
if (def.camera.perspective) {
  camera = perspectiveCamera
} else {
  camera = orthographicCamera
}
camera.perspective = def.camera.perspective
camera.centerX = def.camera.centerX
camera.centerY = def.camera.centerY
camera.centerZ = def.camera.centerZ
camera.eyeX = def.camera.eyeX
camera.eyeY = def.camera.eyeY
camera.eyeZ = def.camera.eyeZ
camera.upX = def.camera.upX
camera.upY = def.camera.upY
camera.upZ = def.camera.upZ
camera.fovy = degToRad(def.camera.fovy)
camera.aspect = def.camera.aspect
camera.near = def.camera.near
camera.far = def.camera.far

// Ambient Light
const ambientLight = new AmbientLight(
  def.ambientLight.color,
  def.ambientLight.ambient,
  def.ambientLight.active
)

// Directional Light
const dirLight = new DirectionalLight(
  def.dirLight.color,
  def.dirLight.ambient,
  def.dirLight.diffuse,
  def.dirLight.specular,
  degToRad(def.dirLight.dx),
  degToRad(def.dirLight.dy),
  degToRad(def.dirLight.dz),
  def.dirLight.active
)

// Point Lights X + orbe X
const pointLightX = new PointLight(
  def.pointLightX.color,
  def.pointLightX.ambient,
  def.pointLightX.diffuse,
  def.pointLightX.specular,
  unormalize(def.pointLightX.px, def.settings.gridSize, 0),
  unormalize(def.pointLightX.py, def.settings.gridSize, 0),
  unormalize(def.pointLightX.pz, def.settings.gridSize, 0),
  def.pointLightX.active,
  def.pointLightX.constant,
  def.pointLightX.linear,
  def.pointLightX.quadratic
)

const pointLightXOrbe = new Sphere()
const pointLightXOrbeMaterial = new Material(def.pointLightX.color)
const pointLightXOrbeMesh = new Mesh(
  pointLightXOrbe, pointLightXOrbeMaterial, gl.TRIANGLES,
  def.pointLightX.active,
  unormalize(def.pointLightX.px, def.settings.gridSize, 0),
  unormalize(def.pointLightX.py, def.settings.gridSize, 0),
  unormalize(def.pointLightX.pz, def.settings.gridSize, 0),
  0, 0, 0, 0.25, 0.25, 0.25
)

// Point Lights Y + orbe Y
const pointLightY = new PointLight(
  def.pointLightY.color,
  def.pointLightY.ambient,
  def.pointLightY.diffuse,
  def.pointLightY.specular,
  unormalize(def.pointLightY.px, def.settings.gridSize, 0),
  unormalize(def.pointLightY.py, def.settings.gridSize, 0),
  unormalize(def.pointLightY.pz, def.settings.gridSize, 0),
  def.pointLightY.active,
  def.pointLightY.constant,
  def.pointLightY.linear,
  def.pointLightY.quadratic
)

const pointLightYOrbe = new Sphere()
const pointLightYOrbeMaterial = new Material(def.pointLightY.color)
const pointLightYOrbeMesh = new Mesh(
  pointLightYOrbe, pointLightYOrbeMaterial, gl.TRIANGLES,
  def.pointLightY.active,
  unormalize(def.pointLightY.px, def.settings.gridSize, 0),
  unormalize(def.pointLightY.py, def.settings.gridSize, 0),
  unormalize(def.pointLightY.pz, def.settings.gridSize, 0),
  0, 0, 0, 0.25, 0.25, 0.25
)

// Point Lights Z + orbe Z
const pointLightZ = new PointLight(
  def.pointLightZ.color,
  def.pointLightZ.ambient,
  def.pointLightZ.diffuse,
  def.pointLightZ.specular,
  unormalize(def.pointLightZ.px, def.settings.gridSize, 0),
  unormalize(def.pointLightZ.py, def.settings.gridSize, 0),
  unormalize(def.pointLightZ.pz, def.settings.gridSize, 0),
  def.pointLightZ.active,
  def.pointLightZ.constant,
  def.pointLightZ.linear,
  def.pointLightZ.quadratic
)

const pointLightZOrbe = new Sphere()
const pointLightZOrbeMaterial = new Material(def.pointLightZ.color)
const pointLightZOrbeMesh = new Mesh(
  pointLightZOrbe, pointLightZOrbeMaterial, gl.TRIANGLES,
  def.pointLightZ.active,
  unormalize(def.pointLightZ.px, def.settings.gridSize, 0),
  unormalize(def.pointLightZ.py, def.settings.gridSize, 0),
  unormalize(def.pointLightZ.pz, def.settings.gridSize, 0),
  0, 0, 0, 0.25, 0.25, 0.25
)

// Spot Light
const spotLight = new SpotLight(
  def.spotLight.color,
  def.spotLight.ambient,
  def.spotLight.diffuse,
  def.spotLight.specular,
  unormalize(def.spotLight.px, def.settings.gridSize, 0),
  unormalize(def.spotLight.py, def.settings.gridSize, 0),
  unormalize(def.spotLight.pz, def.settings.gridSize, 0),
  degToRad(def.spotLight.dx),
  degToRad(def.spotLight.dy),
  degToRad(def.spotLight.dz),
  def.spotLight.active,
  degToRad(def.spotLight.focus),
  degToRad(def.spotLight.cutOff),
  def.spotLight.constant,
  def.spotLight.linear,
  def.spotLight.quadratic
)

const spotLightOrbe = new Sphere()
const spotLightOrbeMaterial = new Material(def.spotLight.color)
const spotLightOrbeMesh = new Mesh(
  spotLightOrbe, spotLightOrbeMaterial, gl.TRIANGLES,
  def.spotLight.active,
  unormalize(def.spotLight.px, def.settings.gridSize, 0),
  unormalize(def.spotLight.py, def.settings.gridSize, 0),
  unormalize(def.spotLight.pz, def.settings.gridSize, 0),
  0, 0, 0, 0.25, 0.25, 0.25
)

// Grid setup
const grid = new Grid(def.settings.gridSize, -0.01)
const gridMaterial = new Material(def.settings.gridColor)
const gridMesh = new Mesh(grid, gridMaterial, gl.LINES, def.settings.gridActive)

// Axis x setup
const xAxis = new AxisX(def.settings.gridSize / 2)
const xAxisMaterial = new Material(def.settings.xAxisColor)
const xAxisMesh = new Mesh(xAxis, xAxisMaterial, gl.LINES, def.settings.xAxisActive)

// Axis y setup
const yAxis = new AxisY(def.settings.gridSize / 2)
const yAxisMaterial = new Material(def.settings.yAxisColor)
const yAxisMesh = new Mesh(yAxis, yAxisMaterial, gl.LINES, def.settings.yAxisActive)

// Axis z setup
const zAxis = new AxisZ(def.settings.gridSize / 2)
const zAxisMaterial = new Material(def.settings.zAxisColor)
const zAxisMesh = new Mesh(zAxis, zAxisMaterial, gl.LINES, def.settings.zAxisActive)

// Sphere setup
const sphere = new Sphere()
const sphereMap = require('./textures/earthmap-1024x512.jpg')
const sphereMaterial = new Material(def.sphere.color, def.sphere.diffuse, def.sphere.specular, def.sphere.shininess, false, sphereMap, def.sphere.useTexture)
const sphereMesh = new Mesh(
  sphere,
  sphereMaterial,
  def.sphere.primitive,
  def.sphere.active,
  unormalize(def.sphere.tx, def.settings.gridSize, 0),
  unormalize(def.sphere.ty, def.settings.gridSize, 0),
  unormalize(def.sphere.tz, def.settings.gridSize, 0),
  degToRad(def.sphere.rx),
  degToRad(def.sphere.ry),
  degToRad(def.sphere.rz),
  unormalize(def.sphere.sx, def.settings.gridSize, 1),
  unormalize(def.sphere.sy, def.settings.gridSize, 1),
  unormalize(def.sphere.sz, def.settings.gridSize, 1)
)

// Sphere Cosmetics
// Axis x setup
const SpherexAxis = new AxisX(2)
const SpherexAxisMaterial = new Material(def.settings.xAxisColor)
const SpherexAxisMesh = new Mesh(
  SpherexAxis,
  SpherexAxisMaterial,
  gl.LINES,
  def.sphere.active,
  unormalize(def.sphere.tx, def.settings.gridSize, 0),
  unormalize(def.sphere.ty, def.settings.gridSize, 0),
  unormalize(def.sphere.tz, def.settings.gridSize, 0),
  degToRad(def.sphere.rx),
  degToRad(def.sphere.ry),
  degToRad(def.sphere.rz),
  unormalize(def.sphere.sx, def.settings.gridSize, 1),
  unormalize(def.sphere.sy, def.settings.gridSize, 1),
  unormalize(def.sphere.sz, def.settings.gridSize, 1)
)

// Axis y setup
const SphereyAxis = new AxisY(2)
const SphereyAxisMaterial = new Material(def.settings.yAxisColor)
const SphereyAxisMesh = new Mesh(
  SphereyAxis,
  SphereyAxisMaterial,
  gl.LINES,
  def.sphere.active,
  unormalize(def.sphere.tx, def.settings.gridSize, 0),
  unormalize(def.sphere.ty, def.settings.gridSize, 0),
  unormalize(def.sphere.tz, def.settings.gridSize, 0),
  degToRad(def.sphere.rx),
  degToRad(def.sphere.ry),
  degToRad(def.sphere.rz),
  unormalize(def.sphere.sx, def.settings.gridSize, 1),
  unormalize(def.sphere.sy, def.settings.gridSize, 1),
  unormalize(def.sphere.sz, def.settings.gridSize, 1)
)

// Axis z setup
const SpherezAxis = new AxisZ(2)
const SpherezAxisMaterial = new Material(def.settings.zAxisColor)
const SpherezAxisMesh = new Mesh(
  SpherezAxis,
  SpherezAxisMaterial,
  gl.LINES,
  def.sphere.active,
  unormalize(def.sphere.tx, def.settings.gridSize, 0),
  unormalize(def.sphere.ty, def.settings.gridSize, 0),
  unormalize(def.sphere.tz, def.settings.gridSize, 0),
  degToRad(def.sphere.rx),
  degToRad(def.sphere.ry),
  degToRad(def.sphere.rz),
  unormalize(def.sphere.sx, def.settings.gridSize, 1),
  unormalize(def.sphere.sy, def.settings.gridSize, 1),
  unormalize(def.sphere.sz, def.settings.gridSize, 1)
)

// BoundingBox setup
const SphereBoundingBox = new SimpleCube()
const SphereBoundingBoxMaterial = new Material(def.settings.gridColor)
const SphereBoundingBoxMesh = new Mesh(
  SphereBoundingBox,
  SphereBoundingBoxMaterial,
  gl.LINES,
  def.sphere.active,
  unormalize(def.sphere.tx, def.settings.gridSize, 0),
  unormalize(def.sphere.ty, def.settings.gridSize, 0),
  unormalize(def.sphere.tz, def.settings.gridSize, 0),
  degToRad(def.sphere.rx),
  degToRad(def.sphere.ry),
  degToRad(def.sphere.rz),
  unormalize(def.sphere.sx, def.settings.gridSize, 1),
  unormalize(def.sphere.sy, def.settings.gridSize, 1),
  unormalize(def.sphere.sz, def.settings.gridSize, 1)
)

sphereMesh.addCosmetic(SpherexAxisMesh)
sphereMesh.addCosmetic(SphereyAxisMesh)
sphereMesh.addCosmetic(SpherezAxisMesh)
sphereMesh.addCosmetic(SphereBoundingBoxMesh)

/*
// Cilinder setup
const cilinder = new Cilinder()
const cilinderMaterial = new Material(rgba(cilinder.color), cilinder.diffuse, cilinder.specular, cilinder.shininess, false, './textures/earthmap-1024x512.jpg', cilinder.useTexture)
const cilinderMesh = new Mesh(
  cilinder,
  cilinderMaterial,
  cilinder.primitive,
  cilinder.active,
  unormalize(cilinder.tx, settings.gridSize, 0),
  unormalize(cilinder.ty, settings.gridSize, 0),
  unormalize(cilinder.tz, settings.gridSize, 0),
  degToRad(cilinder.rx),
  degToRad(cilinder.ry),
  degToRad(cilinder.rz),
  unormalize(cilinder.sx, settings.gridSize, 1),
  unormalize(cilinder.sy, settings.gridSize, 1),
  unormalize(cilinder.sz, settings.gridSize, 1)
)

// Cube setup
const cube = new Cube()
const cubeMaterial = new Material(rgba(cube.color), cube.diffuse, cube.specular, cube.shininess, false, './textures/earthmap-1024x512.jpg', cube.useTexture)
const cubeMesh = new Mesh(
  cube,
  cubeMaterial,
  cube.primitive,
  cube.active,
  unormalize(cube.tx, settings.gridSize, 0),
  unormalize(cube.ty, settings.gridSize, 0),
  unormalize(cube.tz, settings.gridSize, 0),
  degToRad(cube.rx),
  degToRad(cube.ry),
  degToRad(cube.rz),
  unormalize(cube.sx, settings.gridSize, 1),
  unormalize(cube.sy, settings.gridSize, 1),
  unormalize(cube.sz, settings.gridSize, 1)
)
*/
// Scene
const scene = new Scene(def.settings.backgroundColor, ambientLight)

// Grid add on scene
scene.addMesh(gridMesh)

// Axis add on scene
scene.addMesh(xAxisMesh)
scene.addMesh(yAxisMesh)
scene.addMesh(zAxisMesh)

// Lights on scene
scene.addDirLight(dirLight)
scene.addPointLight(pointLightX)
scene.addPointLight(pointLightY)
scene.addPointLight(pointLightZ)
scene.addSpotLight(spotLight)

// Orbs add on scene
scene.addMesh(pointLightXOrbeMesh)
scene.addMesh(pointLightYOrbeMesh)
scene.addMesh(pointLightZOrbeMesh)
scene.addMesh(spotLightOrbeMesh)

// Meshes add on scene
scene.addMesh(sphereMesh)
// scene.addMesh(cilinderMesh)
// scene.addMesh(cubeMesh)

// GUI
const guiControls = new dat.GUI()

// Settings Folder

const globalSettingsFolder = guiControls.addFolder('Global Settings')
globalSettingsFolder.addColor(scene, 'clearColor')
const xAxisFolder = guiControls.addFolder('X Axis')
xAxisFolder.add(xAxisMesh, 'active')
xAxisFolder.addColor(xAxisMaterial, 'color')
const yAxisFolder = guiControls.addFolder('Y Axis')
yAxisFolder.add(yAxisMesh, 'active')
yAxisFolder.addColor(yAxisMaterial, 'color')
const zAxisFolder = guiControls.addFolder('Z Axis')
zAxisFolder.add(zAxisMesh, 'active')
zAxisFolder.addColor(zAxisMaterial, 'color')
const gridFolder = guiControls.addFolder('Grid')
gridFolder.add(gridMesh, 'active')
gridFolder.addColor(gridMaterial, 'color')
// settingsFolder.add(settings, 'gridSize', 0, 100, 2)

// Ambient Light Folder
const ambientLightFolder = guiControls.addFolder('Ambient Light')
ambientLightFolder.add(ambientLight, 'active')
ambientLightFolder.addColor(ambientLight, 'color')
ambientLightFolder.add(ambientLight, 'ambient', 0, 1, 0.001)

// Directional Light Folder
const dirLightFolder = guiControls.addFolder('Directional Light')
dirLightFolder.add(dirLight, 'active')
dirLightFolder.addColor(dirLight, 'color')
dirLightFolder.add(dirLight, 'ambient', 0, 1, 0.001)
dirLightFolder.add(dirLight, 'diffuse', 0, 1, 0.001)
dirLightFolder.add(dirLight, 'specular', 0, 1, 0.001)
dirLightFolder.add(dirLight, 'dx', -360, 360, 0.001)
dirLightFolder.add(dirLight, 'dy', -360, 360, 0.001)
dirLightFolder.add(dirLight, 'dz', -360, 360, 0.001)

// Point Light X
const pointLightXFolder = guiControls.addFolder('Point Light X')
pointLightXFolder.add(pointLightX, 'active')
pointLightXFolder.addColor(pointLightX, 'color')
pointLightXFolder.add(pointLightX, 'ambient', 0, 1, 0.001)
pointLightXFolder.add(pointLightX, 'diffuse', 0, 1, 0.001)
pointLightXFolder.add(pointLightX, 'specular', 0, 1, 0.001)
pointLightXFolder.add(pointLightX, 'px', -1, 1, 0.001)
pointLightXFolder.add(pointLightX, 'py', -1, 1, 0.001)
pointLightXFolder.add(pointLightX, 'pz', -1, 1, 0.001)
pointLightXFolder.add(pointLightX, 'constant', 0, 1, 0.001)
pointLightXFolder.add(pointLightX, 'linear', 0, 1, 0.001)
pointLightXFolder.add(pointLightX, 'quadratic', 0, 1, 0.001)

// Point Light Y
const pointLightYFolder = guiControls.addFolder('Point Light Y')
pointLightYFolder.add(pointLightY, 'active')
pointLightYFolder.addColor(pointLightY, 'color')
pointLightYFolder.add(pointLightY, 'ambient', 0, 1, 0.001)
pointLightYFolder.add(pointLightY, 'diffuse', 0, 1, 0.001)
pointLightYFolder.add(pointLightY, 'specular', 0, 1, 0.001)
pointLightYFolder.add(pointLightY, 'px', -1, 1, 0.001)
pointLightYFolder.add(pointLightY, 'py', -1, 1, 0.001)
pointLightYFolder.add(pointLightY, 'pz', -1, 1, 0.001)
pointLightYFolder.add(pointLightY, 'constant', 0, 1, 0.001)
pointLightYFolder.add(pointLightY, 'linear', 0, 1, 0.001)
pointLightYFolder.add(pointLightY, 'quadratic', 0, 1, 0.001)

// Point Light Z
const pointLightZFolder = guiControls.addFolder('Point Light Z')
pointLightZFolder.add(pointLightZ, 'active')
pointLightZFolder.addColor(pointLightZ, 'color')
pointLightZFolder.add(pointLightZ, 'ambient', 0, 1, 0.001)
pointLightZFolder.add(pointLightZ, 'diffuse', 0, 1, 0.001)
pointLightZFolder.add(pointLightZ, 'specular', 0, 1, 0.001)
pointLightZFolder.add(pointLightZ, 'px', -1, 1, 0.001)
pointLightZFolder.add(pointLightZ, 'py', -1, 1, 0.001)
pointLightZFolder.add(pointLightZ, 'pz', -1, 1, 0.001)
pointLightZFolder.add(pointLightZ, 'constant', 0, 1, 0.001)
pointLightZFolder.add(pointLightZ, 'linear', 0, 1, 0.001)
pointLightZFolder.add(pointLightZ, 'quadratic', 0, 1, 0.001)

// Spot Light
const spotLightFolder = guiControls.addFolder('Spot Light')
spotLightFolder.add(spotLight, 'active')
spotLightFolder.addColor(spotLight, 'color')
spotLightFolder.add(spotLight, 'ambient', 0, 1, 0.001)
spotLightFolder.add(spotLight, 'diffuse', 0, 1, 0.001)
spotLightFolder.add(spotLight, 'specular', 0, 1, 0.001)
spotLightFolder.add(spotLight, 'px', -1, 1, 0.001)
spotLightFolder.add(spotLight, 'py', -1, 1, 0.001)
spotLightFolder.add(spotLight, 'pz', -1, 1, 0.001)
spotLightFolder.add(spotLight, 'dx', -360, 360, 0.001)
spotLightFolder.add(spotLight, 'dy', -360, 360, 0.001)
spotLightFolder.add(spotLight, 'dz', -360, 360, 0.001)
spotLightFolder.add(spotLight, 'focus', -360, 360, 0.001)
spotLightFolder.add(spotLight, 'cutOff', -360, 360, 0.001)
spotLightFolder.add(spotLight, 'constant', 0, 1, 0.001)
spotLightFolder.add(spotLight, 'linear', 0, 1, 0.001)
spotLightFolder.add(spotLight, 'quadratic', 0, 1, 0.001)

// Camera Folder
const cameraFolder = guiControls.addFolder('Camera')
// cameraFolder.add(camera, 'perspective')
cameraFolder.add(camera, 'centerX', -360, 360, 0.001)
cameraFolder.add(camera, 'centerY', -360, 360, 0.001)
cameraFolder.add(camera, 'centerZ', -360, 360, 0.001)
cameraFolder.add(camera, 'eyeX', -360, 360, 0.001)
cameraFolder.add(camera, 'eyeY', -360, 360, 0.001)
cameraFolder.add(camera, 'eyeZ', -360, 360, 0.001)
cameraFolder.add(camera, 'upX', -360, 360, 0.001)
cameraFolder.add(camera, 'upY', -360, 360, 0.001)
cameraFolder.add(camera, 'upZ', -360, 360, 0.001)

cameraFolder.add(camera, 'fovy', -180, 180, 0.001)
cameraFolder.add(camera, 'aspect', 0, 2 * gl.canvas.width / gl.canvas.height, 0.001)

if (def.camera.perspective) {
  cameraFolder.add(camera, 'near', 0, 1000, 0.001)
  cameraFolder.add(camera, 'far', 0, 1000, 0.001)
} else {
  cameraFolder.add(camera, 'left', -100, 100, 0.001)
  cameraFolder.add(camera, 'right', -100, 100, 0.001)
  cameraFolder.add(camera, 'bottom', -100, 100, 0.001)
  cameraFolder.add(camera, 'top', -100, 100, 0.001)
}

// Sphere Folder
const sphereFolder = guiControls.addFolder('Sphere')
sphereFolder.add(sphereMesh, 'active')
sphereFolder.add(sphereMesh.material, 'useTexture')
sphereFolder.add(sphereMesh, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
sphereFolder.addColor(sphereMesh.material, 'color')
sphereFolder.add(sphereMesh.material, 'diffuse', 0, 1, 0.001)
sphereFolder.add(sphereMesh.material, 'specular', 0, 1, 0.001)
sphereFolder.add(sphereMesh.material, 'shininess', 1, 128, 0.001)
sphereFolder.add(sphereMesh, 'tx', -1, 2, 0.001)
sphereFolder.add(sphereMesh, 'ty', -1, 2, 0.001)
sphereFolder.add(sphereMesh, 'tz', -1, 2, 0.001)
sphereFolder.add(sphereMesh, 'rx', 0, 360, 0.1)
sphereFolder.add(sphereMesh, 'ry', 0, 360, 0.1)
sphereFolder.add(sphereMesh, 'rz', 0, 360, 0.1)
sphereFolder.add(sphereMesh, 'sx', -1, 2, 0.001)
sphereFolder.add(sphereMesh, 'sy', -1, 2, 0.001)
sphereFolder.add(sphereMesh, 'sz', -1, 2, 0.001)
/*
// Cilinder Folder
const cilinderFolder = guiControls.addFolder('Cilinder')
cilinderFolder.add(cilinder, 'active')
cilinderFolder.add(cilinder, 'useTexture')
cilinderFolder.add(cilinder, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
cilinderFolder.addColor(cilinder, 'color')
cilinderFolder.add(cilinder, 'diffuse', 0, 1, 0.001)
cilinderFolder.add(cilinder, 'specular', 0, 1, 0.001)
cilinderFolder.add(cilinder, 'shininess', 1, 128, 0.001)
cilinderFolder.add(cilinder, 'tx', -1, 1, 0.001)
cilinderFolder.add(cilinder, 'ty', -1, 1, 0.001)
cilinderFolder.add(cilinder, 'tz', -1, 1, 0.001)
cilinderFolder.add(cilinder, 'rx', 0, 360, 0.1)
cilinderFolder.add(cilinder, 'ry', 0, 360, 0.1)
cilinderFolder.add(cilinder, 'rz', 0, 360, 0.1)
cilinderFolder.add(cilinder, 'sx', -1, 2, 0.001)
cilinderFolder.add(cilinder, 'sy', -1, 2, 0.001)
cilinderFolder.add(cilinder, 'sz', -1, 2, 0.001)

// Cube Folder
const cubeFolder = guiControls.addFolder('Cube')
cubeFolder.add(cube, 'active')
cubeFolder.add(cube, 'useTexture')
cubeFolder.add(cube, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
cubeFolder.addColor(cube, 'color')
cubeFolder.add(cube, 'diffuse', 0, 1, 0.001)
cubeFolder.add(cube, 'specular', 0, 1, 0.001)
cubeFolder.add(cube, 'shininess', 1, 128, 0.001)
cubeFolder.add(cube, 'tx', -1, 1, 0.001)
cubeFolder.add(cube, 'ty', -1, 1, 0.001)
cubeFolder.add(cube, 'tz', -1, 1, 0.001)
cubeFolder.add(cube, 'rx', 0, 360, 0.1)
cubeFolder.add(cube, 'ry', 0, 360, 0.1)
cubeFolder.add(cube, 'rz', 0, 360, 0.1)
cubeFolder.add(cube, 'sx', -1, 2, 0.001)
cubeFolder.add(cube, 'sy', -1, 2, 0.001)
cubeFolder.add(cube, 'sz', -1, 2, 0.001)
*/
// Rendering scene
const webGLInstance = new WebGLRenderer(canvas)

webGLInstance.previousDelta = 0
webGLInstance.fpsLimit = 30

function render (currentDelta) {
  webGLInstance.updateId = requestAnimationFrame(render)
  const delta = currentDelta - webGLInstance.previousDelta
  if (webGLInstance.fpsLimit && delta < 1000 / webGLInstance.fpsLimit) {
    return
  }

  updateCosmetics(scene.meshes)

  webGLInstance.render(scene, camera)

  webGLInstance.previousDelta = currentDelta
}

function updateCosmetics (meshes) {
  for (var i = 0; i < meshes.length; i++) {
    const mesh = meshes[i]
    for (var j = 0; j < mesh.cosmetics.length; j++) {
      const cosmetic = mesh.cosmetics[j]
      cosmetic.active = mesh.active
      cosmetic.tx = mesh.tx
      cosmetic.ty = mesh.ty
      cosmetic.tz = mesh.tz
      cosmetic.rx = mesh.rx
      cosmetic.ry = mesh.ry
      cosmetic.rz = mesh.rz
      cosmetic.sx = mesh.sx
      cosmetic.sy = mesh.sy
      cosmetic.sz = mesh.sz
    }
  }
}

/*
// Mouse events
var mouse = require('mouse-event')
// Mouse event listener
window.addEventListener('mousemove', function (ev) {
  console.log(mouse.buttons(ev), mouse.x(ev), mouse.y(ev))
})
*/

// Forst call for rendering loop
window.requestAnimationFrame(render)

/*
TODO
-> MAPEAR TEXTURAS A CUBO Y CILINDRO
    -> cargar las texturas
    -> mapear las coordenadad
-> SOLUCIONAR EL TEMA DE LOS NORMALIZE
    -> normalizar los valores en config
    -> ver limites en la gui
-> MOVER ORBES con las luces
*/
