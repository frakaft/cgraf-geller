const normalize = (a) => {
  const length = Math.sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2])
  return [a[0] / length, a[1] / length, a[2] / length]
}

const cross = (a, b) => {
  return [a[1] * b[2] - a[2] * b[1], a[2] * b[0] + a[0] * b[2], a[0] * b[1] - a[1] * b[0]]
}

const normals = (a, b, c) => {
  const va = [a[0] - c[0], a[1] - c[1], a[2] - c[2]]
  const vb = [b[0] - c[0], b[1] - c[1], b[2] - c[2]]
  return normalize(cross(va, vb))
}

module.exports = {
  normalize,
  cross,
  normals
}
