const Hex = require('./hex')

class Style extends Hex {
  constructor (styleString) {
    styleString = styleString.substr(1)
    styleString = styleString.length === 3 ? styleString[0] + styleString[0] + styleString[1] + styleString[1] + styleString[2] + styleString[2] : styleString
    super(parseInt(styleString, 16))
  }
}

module.exports = Style
