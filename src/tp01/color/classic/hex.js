const RGB = require('./rgb')

class Hex extends RGB {
  constructor (hexNumber) {
    const r = (hexNumber >> 16 & 0xff) / 255
    const g = (hexNumber >> 8 & 0xff) / 255
    const b = (hexNumber & 0xff) / 255
    super(r, g, b)
  }
}

module.exports = Hex
