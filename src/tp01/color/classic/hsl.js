const RGBA = require('./rgba')

class HSL extends RGBA {
  constructor (h, s, l) {
    h = h < 0 ? 360 - (h % 360) : h % 360
    s = Math.min(Math.max(s, 0), 1)
    l = Math.min(Math.max(l, 0), 1)

    let c = (1 - Math.abs(2 * l - 1)) * s
    let x = c * (1 - Math.abs(((h / 60) % 2) - 1))
    let m = l - c / 2

    let r, g, b

    if (h >= 0 && h < 60) {
      r = c + m
      g = x + m
      b = 0 + m
    } else if (h >= 60 && h < 120) {
      r = x + m
      g = c + m
      b = 0 + m
    } else if (h >= 120 && h < 180) {
      r = 0 + m
      g = c + m
      b = x + m
    } else if (h >= 180 && h < 240) {
      r = 0 + m
      g = x + m
      b = c + m
    } else if (h >= 240 && h < 300) {
      r = x + m
      g = 0 + m
      b = c + m
    } else if (h >= 300 && h < 360) {
      r = c + m
      g = 0 + m
      b = x + m
    }
    super(r, g, b, 1)
  }
}

module.exports = HSL
