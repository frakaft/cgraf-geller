class RGBA {
  constructor (r = 1, g = 1, b = 1, a = 1) {
    this.r = Math.min(Math.max(r, 0), 1)
    this.g = Math.min(Math.max(g, 0), 1)
    this.b = Math.min(Math.max(b, 0), 1)
    this.a = Math.min(Math.max(a, 0), 1)
  }

  vec4 () {
    return [this.r, this.g, this.b, this.a]
  }

  vec3 () {
    return [this.r, this.g, this.b]
  }
}

module.exports = RGBA
