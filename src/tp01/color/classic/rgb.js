const RGBA = require('./rgba')

class RGB extends RGBA {
  constructor (r, g, b) {
    super(r, g, b, 1)
  }
}

module.exports = RGB
