const transpose = (a) => {
  let mtx = []
  for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 4; j++) {
      mtx.push(a[i + j * 4])
    }
  }

  return mtx
}

const multiply = (a, b) => {
  let mtx = []
  for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 4; j++) {
      let aux = 0
      for (var k = 0; k < 4; k++) {
        aux += a[k + i * 4] * b[j + k * 4]
      }
      mtx.push(aux)
    }
  }
  return mtx
}

module.exports = {
  transpose,
  multiply
}
