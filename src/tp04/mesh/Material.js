class Material {
  constructor (color, diffuse = undefined, specular = undefined, shininess = undefined, obscure = true) {
    this.color = color
    this.diffuse = diffuse
    this.specular = specular
    this.shininess = shininess
    this.obscure = obscure
  }
  getDiffuse () {
    return [this.color[0] * this.diffuse, this.color[1] * this.diffuse, this.color[2] * this.diffuse]
  }
  getSpecular () {
    return [this.color[0] * this.specular, this.color[1] * this.specular, this.color[2] * this.specular]
  }
}

module.exports = Material
