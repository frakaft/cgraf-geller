const Vector = require('./Vector')

class AxisX extends Vector {
  constructor (size) {
    const axis = [
      0.0, 0.0, 0.0,
      size, 0.0, 0.0
    ]

    super(axis)
  }
}
class AxisY extends Vector {
  constructor (size) {
    const axis = [
      0.0, 0.0, 0.0,
      0.0, size, 0.0
    ]

    super(axis)
  }
}
class AxisZ extends Vector {
  constructor (size) {
    const axis = [
      0.0, 0.0, 0.0,
      0.0, 0.0, size
    ]

    super(axis)
  }
}

module.exports = {
  AxisX,
  AxisY,
  AxisZ
}
