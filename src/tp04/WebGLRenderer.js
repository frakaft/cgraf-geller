const vsSourceLight = require('./shaders/vertex-shader-light.glsl')
const fsSourceLight = require('./shaders/fragment-shader-light.glsl')
const vsSource = require('./shaders/vertex-shader.glsl')
const fsSource = require('./shaders/fragment-shader.glsl')
const twgl = require('twgl.js')

class WebGLRenderer {
  constructor (canvas) {
    this.gl = canvas.getContext('webgl')
    this.programObscure = twgl.createProgramInfo(this.gl, [vsSource, fsSource])
    this.programLight = twgl.createProgramInfo(this.gl, [vsSourceLight, fsSourceLight])
  }
  render (scene, camera) {
    const gl = this.gl
    gl.clearColor(scene.clearColor[0], scene.clearColor[1], scene.clearColor[2], scene.clearColor[3])
    gl.enable(gl.DEPTH_TEST)
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
    twgl.resizeCanvasToDisplaySize(gl.canvas)
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight)

    for (let i = 0; i < scene.meshes.length; i++) {
      const mesh = scene.meshes[i]
      if (mesh.active) {
        mesh.program = mesh.material.obscure ? this.programObscure : this.programLight
        mesh.data = mesh.material.obscure ? {
          aPosition: mesh.VBOData,
          indices: mesh.IBOData
        } : {
          aPosition: mesh.VBOData,
          indices: mesh.IBOData,
          aNormal: mesh.normals
        }
        mesh.uniforms = mesh.material.obscure ? {
          uColor: mesh.material.color,
          uModelMatrix: mesh.getModelMatrix(),
          uViewMatrix: camera.getViewMatrix(),
          uProjectionMatrix: camera.getProjectionMatrix()
        } : {
          uModelMatrix: mesh.getModelMatrix(),
          uNormalMatrix: mesh.getNormalMatrix(),
          uViewMatrix: camera.getViewMatrix(),
          uProjectionMatrix: camera.getProjectionMatrix(),
          uViewPos: camera.getViewPos(),
          'material.diffuse': mesh.material.getDiffuse(),
          'material.specular': mesh.material.getSpecular(),
          'material.shininess': mesh.material.shininess,
          'ambientLight.active': scene.ambientLight.active,
          'ambientLight.color': scene.ambientLight.getAmbient(),

          'dirLights[0].active': scene.dirLights[0].active,
          'dirLights[0].direction': scene.dirLights[0].getDirection(),
          'dirLights[0].ambient': scene.dirLights[0].getAmbient(),
          'dirLights[0].diffuse': scene.dirLights[0].getDiffuse(),
          'dirLights[0].specular': scene.dirLights[0].getSpecular(),

          'pointLights[0].active': scene.pointLights[0].active,
          'pointLights[0].position': scene.pointLights[0].getPosition(),
          'pointLights[0].ambient': scene.pointLights[0].getAmbient(),
          'pointLights[0].diffuse': scene.pointLights[0].getDiffuse(),
          'pointLights[0].specular': scene.pointLights[0].getSpecular(),
          'pointLights[0].constant': scene.pointLights[0].constant,
          'pointLights[0].linear': scene.pointLights[0].linear,
          'pointLights[0].quadratic': scene.pointLights[0].quadratic,

          'pointLights[1].active': scene.pointLights[1].active,
          'pointLights[1].position': scene.pointLights[1].getPosition(),
          'pointLights[1].ambient': scene.pointLights[1].getAmbient(),
          'pointLights[1].diffuse': scene.pointLights[1].getDiffuse(),
          'pointLights[1].specular': scene.pointLights[1].getSpecular(),
          'pointLights[1].constant': scene.pointLights[1].constant,
          'pointLights[1].linear': scene.pointLights[1].linear,
          'pointLights[1].quadratic': scene.pointLights[1].quadratic,

          'pointLights[2].active': scene.pointLights[2].active,
          'pointLights[2].position': scene.pointLights[2].getPosition(),
          'pointLights[2].ambient': scene.pointLights[2].getAmbient(),
          'pointLights[2].diffuse': scene.pointLights[2].getDiffuse(),
          'pointLights[2].specular': scene.pointLights[2].getSpecular(),
          'pointLights[2].constant': scene.pointLights[2].constant,
          'pointLights[2].linear': scene.pointLights[2].linear,
          'pointLights[2].quadratic': scene.pointLights[2].quadratic,

          'spotLights[0].active': scene.spotLights[0].active,
          'spotLights[0].position': scene.spotLights[0].getPosition(),
          'spotLights[0].direction': scene.spotLights[0].getDirection(),
          'spotLights[0].cutOff': scene.spotLights[0].getCutOff(),
          'spotLights[0].outerCutOff': scene.spotLights[0].getOuterCutOff(),
          'spotLights[0].ambient': scene.spotLights[0].getAmbient(),
          'spotLights[0].diffuse': scene.spotLights[0].getDiffuse(),
          'spotLights[0].specular': scene.spotLights[0].getSpecular(),
          'spotLights[0].constant': scene.spotLights[0].constant,
          'spotLights[0].linear': scene.spotLights[0].linear,
          'spotLights[0].quadratic': scene.spotLights[0].quadratic
        }
        const bufferInfo = twgl.createBufferInfoFromArrays(this.gl, mesh.data)
        gl.useProgram(mesh.program.program)
        twgl.setBuffersAndAttributes(gl, mesh.program, bufferInfo)
        twgl.setUniforms(mesh.program, mesh.uniforms)
        twgl.drawBufferInfo(gl, bufferInfo, mesh.primitive)
      }
    }
  }
}
module.exports = WebGLRenderer
