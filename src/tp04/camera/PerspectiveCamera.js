const Camera = require('./Camera')
const glMatrix = require('gl-matrix')

class PerspectiveCamera extends Camera {
  constructor (fovy, aspect, near, far) {
    super()
    this.fovy = fovy
    this.aspect = aspect
    this.near = near
    this.far = far
    this.projectionMatrix = glMatrix.mat4.create()
  }
  getProjectionMatrix () {
    glMatrix.mat4.identity(this.projectionMatrix)
    glMatrix.mat4.perspective(this.projectionMatrix, this.fovy, this.aspect, this.near, this.far)
    return this.projectionMatrix
  }
}

module.exports = PerspectiveCamera
