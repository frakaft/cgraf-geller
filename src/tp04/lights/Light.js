class Light {
  constructor (color, active = true) {
    this.color = color
    this.active = active
  }
}

module.exports = Light
