/**
 * Resize canvas if needed
 */
function resizeCanvas (canvas) {
  var realToCSSPixels = window.devicePixelRatio
  // Lookup the size the browser is displaying the canvas in CSS pixels
  // and compute a size needed to make our drawingbuffer match it in
  // device pixels.
  var displayWidth = Math.floor(canvas.clientWidth * realToCSSPixels)
  var displayHeight = Math.floor(canvas.clientHeight * realToCSSPixels)

  // Check if the canvas is not the same size.
  if (canvas.width !== displayWidth ||
      canvas.height !== displayHeight) {
    // Make the canvas the same size
    canvas.width = displayWidth
    canvas.height = displayHeight
  }
}

/**
 * Disables the right click menu for the given element.
 */
function disableRightClickContextMenu (element) {
  element.addEventListener('contextmenu', function (e) {
    if (e.button === 2) {
      // Block right-click menu thru preventing default action.
      e.preventDefault()
    }
  })
}

/**
 * rgb in 255 format to rgba normaliced format.
 */
function rgba (rgb) {
  return [rgb[0] / 255, rgb[1] / 255, rgb[2] / 255, 1]
}

/**
 * rgba . scalar
 */
function rgbaXscalar (rgba, scalar) {
  return [rgba[0] * scalar, rgba[1] * scalar, rgba[2] * scalar, rgba[3]]
}

/**
 * Rad to Deg.
 */
function radToDeg (r) {
  return r * 180 / Math.PI
}

/**
 * Deg to Rad.
 */
function degToRad (d) {
  return d * Math.PI / 180
}

/**
 * unormalize value
 */
function unormalize (value, maxValue, normalValue) {
  return (value * maxValue) + normalValue
}

module.exports = {
  resizeCanvas,
  disableRightClickContextMenu,
  rgba,
  rgbaXscalar,
  radToDeg,
  degToRad,
  unormalize
}
