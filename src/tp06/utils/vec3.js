const normalize = (v) => {
  const norm = Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2])
  if (norm === 0.0) return [0.0, 0.0, 0.0]
  else return [v[0] / norm, v[1] / norm, v[2] / norm]
}

const cross = (a, b) => {
  return [a[1] * b[2] - a[2] * b[1], a[2] * b[0] + a[0] * b[2], a[0] * b[1] - a[1] * b[0]]
}

const normals = (a, b, c) => {
  let v1 = subtraction(b, a)
  let v2 = subtraction(c, a)
  return normalize(cross(v1, v2))
}

const subtraction = (a, b) => {
  return [a[0] - b[0], a[1] - b[1], a[2] - b[2]]
}

const addition = (a, b) => {
  return [a[0] + b[0], a[1] + b[1], a[2] + b[2]]
}

module.exports = {
  normalize,
  cross,
  normals,
  subtraction,
  addition
}
