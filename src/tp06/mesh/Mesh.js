const glMatrix = require('gl-matrix')

class Mesh {
  constructor (geometry, material, primitive, active = true, tx = 0, ty = 0, tz = 0, rx = 0, ry = 0, rz = 0, sx = 1, sy = 1, sz = 1) {
    this.geometry = geometry
    this.material = material
    this.modelMatrix = glMatrix.mat4.create()
    this.normalMatrix = glMatrix.mat4.create()
    this.primitive = primitive
    this.active = active

    // OpenGL Vertex Buffer Object
    this.VBOData = this.geometry.vertices

    // OpenGL Index Buffer Object
    this.IBOData = this.geometry.faces

    // Normals
    this.normals = this.geometry.getNormals()

    // Translation
    this.tx = tx
    this.ty = ty
    this.tz = tz

    // Rotation
    this.rx = rx
    this.ry = ry
    this.rz = rz

    // Scale
    this.sx = sx
    this.sy = sy
    this.sz = sz
  }
  getModelMatrix () {
    glMatrix.mat4.identity(this.modelMatrix)
    glMatrix.mat4.translate(
      this.modelMatrix,
      this.modelMatrix,
      [this.tx, this.ty, this.tz]
    )
    glMatrix.mat4.rotateX(
      this.modelMatrix,
      this.modelMatrix,
      this.rx
    )
    glMatrix.mat4.rotateY(
      this.modelMatrix,
      this.modelMatrix,
      this.ry
    )
    glMatrix.mat4.rotateZ(
      this.modelMatrix,
      this.modelMatrix,
      this.rz
    )
    glMatrix.mat4.scale(
      this.modelMatrix,
      this.modelMatrix,
      [this.sx, this.sy, this.sz]
    )
    return this.modelMatrix
  }
  getNormalMatrix () {
    glMatrix.mat4.identity(this.modelMatrix)
    glMatrix.mat4.translate(
      this.modelMatrix,
      this.modelMatrix,
      [this.tx, this.ty, this.tz]
    )
    glMatrix.mat4.rotateX(
      this.modelMatrix,
      this.modelMatrix,
      this.rx
    )
    glMatrix.mat4.rotateY(
      this.modelMatrix,
      this.modelMatrix,
      this.ry
    )
    glMatrix.mat4.rotateZ(
      this.modelMatrix,
      this.modelMatrix,
      this.rz
    )
    glMatrix.mat4.scale(
      this.modelMatrix,
      this.modelMatrix,
      [this.sx, this.sy, this.sz]
    )
    glMatrix.mat4.invert(
      this.normalMatrix,
      this.modelMatrix
    )
    glMatrix.mat4.transpose(
      this.normalMatrix,
      this.normalMatrix
    )
    return this.normalMatrix
  }
}

module.exports = Mesh
