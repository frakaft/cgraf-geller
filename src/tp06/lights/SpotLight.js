const Light = require('./Light')

class SpotLight extends Light {
  constructor (color, ambient, diffuse, specular, px, py, pz, dx, dy, dz, active = true, focus = 0.02, cutOff = 0.87, constant = 1.0, linear = 0.09, quadratic = 0.032) {
    super(color, active)
    this.ambient = ambient
    this.diffuse = diffuse
    this.specular = specular
    this.px = px
    this.py = py
    this.pz = pz
    this.dx = dx
    this.dy = dy
    this.dz = dz
    this.focus = focus
    this.cutOff = cutOff
    this.constant = constant
    this.linear = linear
    this.quadratic = quadratic
  }
  getAmbient () {
    return [this.color[0] * this.ambient, this.color[1] * this.ambient, this.color[2] * this.ambient]
  }
  getDiffuse () {
    return [this.color[0] * this.diffuse, this.color[1] * this.diffuse, this.color[2] * this.diffuse]
  }
  getSpecular () {
    return [this.color[0] * this.specular, this.color[1] * this.specular, this.color[2] * this.specular]
  }
  getCutOff () {
    return this.cutOff
  }
  getOuterCutOff () {
    return this.cutOff - this.focus
  }
  getPosition () {
    return [this.px, this.py, this.pz]
  }
  getDirection () {
    return [this.dx, this.dy, this.dz]
  }
}

module.exports = SpotLight
