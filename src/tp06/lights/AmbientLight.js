const Light = require('./Light')

class AmbientLight extends Light {
  constructor (color, ambient, active = true) {
    super(color, active)
    this.ambient = ambient
  }
  getAmbient () {
    return [this.color[0] * this.ambient, this.color[1] * this.ambient, this.color[2] * this.ambient]
  }
}

module.exports = AmbientLight
