const Mesh = require('./mesh/Mesh')
const WebGLRenderer = require('./WebGLRenderer')
const Scene = require('./Scene')
const PerspectiveCamera = require('./camera/PerspectiveCamera')
const OrthographicCamera = require('./camera/OrthographicCamera')
const Grid = require('./geometries/Grid')
const Sphere = require('./geometries/Sphere')
const Material = require('./mesh/Material')
const AmbientLight = require('./lights/AmbientLight')
const DirectionalLight = require('./lights/DirectionalLight')
const PointLight = require('./lights/PointLight')
const SpotLight = require('./lights/SpotLight')
const { AxisX, AxisY, AxisZ } = require('./geometries/Axis')
const { rgba, degToRad, disableRightClickContextMenu, unormalize } = require('./utils/utils')
const { state } = require('./gui/gui')
const WavefrontObj = require('./geometries/WavefrontObj')

const canvas = document.getElementById('c')
const gl = canvas.getContext('webgl')
disableRightClickContextMenu(canvas) // for mouse events

// Render
function render () {
  // Camera setup
  let camera
  if (state.camera.perspective) {
    camera = new PerspectiveCamera(degToRad(state.camera.fovy), state.camera.aspect, state.camera.near, state.camera.far)
  } else {
    camera = new OrthographicCamera(state.camera.left, state.camera.right, state.camera.bottom, state.camera.top, state.camera.near, state.camera.far)
  }
  camera.centerX = state.camera.centerX
  camera.centerY = state.camera.centerY
  camera.centerZ = state.camera.centerZ
  camera.eyeX = state.camera.eyeX
  camera.eyeY = state.camera.eyeY
  camera.eyeZ = state.camera.eyeZ
  camera.upX = state.camera.upX
  camera.upY = state.camera.upY
  camera.upZ = state.camera.upZ
  camera.fovy = degToRad(state.camera.fovy)
  camera.aspect = state.camera.aspect
  camera.near = state.camera.near
  camera.far = state.camera.far

  // Ambient Light
  const ambientLight = new AmbientLight(
    rgba(state.ambientLight.color),
    state.ambientLight.ambient,
    state.ambientLight.active
  )

  // Directional Light
  const dirLight = new DirectionalLight(
    rgba(state.dirLight.color),
    state.dirLight.ambient,
    state.dirLight.diffuse,
    state.dirLight.specular,
    degToRad(state.dirLight.dx),
    degToRad(state.dirLight.dy),
    degToRad(state.dirLight.dz),
    state.dirLight.active
  )

  // Point Lights X + orbe X
  const pointLightX = new PointLight(
    rgba(state.pointLightX.color),
    state.pointLightX.ambient,
    state.pointLightX.diffuse,
    state.pointLightX.specular,
    unormalize(state.pointLightX.px, state.settings.gridSize, 0),
    unormalize(state.pointLightX.py, state.settings.gridSize, 0),
    unormalize(state.pointLightX.pz, state.settings.gridSize, 0),
    state.pointLightX.active,
    state.pointLightX.constant,
    state.pointLightX.linear,
    state.pointLightX.quadratic
  )

  const pointLightXOrbe = new Sphere()
  const pointLightXOrbeMaterial = new Material(rgba(state.pointLightX.color))
  const pointLightXOrbeMesh = new Mesh(
    pointLightXOrbe, pointLightXOrbeMaterial, gl.TRIANGLES,
    state.pointLightX.active,
    unormalize(state.pointLightX.px, state.settings.gridSize, 0),
    unormalize(state.pointLightX.py, state.settings.gridSize, 0),
    unormalize(state.pointLightX.pz, state.settings.gridSize, 0),
    0, 0, 0, 0.25, 0.25, 0.25
  )

  // Point Lights Y + orbe Y
  const pointLightY = new PointLight(
    rgba(state.pointLightY.color),
    state.pointLightY.ambient,
    state.pointLightY.diffuse,
    state.pointLightY.specular,
    unormalize(state.pointLightY.px, state.settings.gridSize, 0),
    unormalize(state.pointLightY.py, state.settings.gridSize, 0),
    unormalize(state.pointLightY.pz, state.settings.gridSize, 0),
    state.pointLightY.active,
    state.pointLightY.constant,
    state.pointLightY.linear,
    state.pointLightY.quadratic
  )

  const pointLightYOrbe = new Sphere()
  const pointLightYOrbeMaterial = new Material(rgba(state.pointLightY.color))
  const pointLightYOrbeMesh = new Mesh(
    pointLightYOrbe, pointLightYOrbeMaterial, gl.TRIANGLES,
    state.pointLightY.active,
    unormalize(state.pointLightY.px, state.settings.gridSize, 0),
    unormalize(state.pointLightY.py, state.settings.gridSize, 0),
    unormalize(state.pointLightY.pz, state.settings.gridSize, 0),
    0, 0, 0, 0.25, 0.25, 0.25
  )

  // Point Lights Z + orbe Z
  const pointLightZ = new PointLight(
    rgba(state.pointLightZ.color),
    state.pointLightZ.ambient,
    state.pointLightZ.diffuse,
    state.pointLightZ.specular,
    unormalize(state.pointLightZ.px, state.settings.gridSize, 0),
    unormalize(state.pointLightZ.py, state.settings.gridSize, 0),
    unormalize(state.pointLightZ.pz, state.settings.gridSize, 0),
    state.pointLightZ.active,
    state.pointLightZ.constant,
    state.pointLightZ.linear,
    state.pointLightZ.quadratic
  )

  const pointLightZOrbe = new Sphere()
  const pointLightZOrbeMaterial = new Material(rgba(state.pointLightZ.color))
  const pointLightZOrbeMesh = new Mesh(
    pointLightZOrbe, pointLightZOrbeMaterial, gl.TRIANGLES,
    state.pointLightZ.active,
    unormalize(state.pointLightZ.px, state.settings.gridSize, 0),
    unormalize(state.pointLightZ.py, state.settings.gridSize, 0),
    unormalize(state.pointLightZ.pz, state.settings.gridSize, 0),
    0, 0, 0, 0.25, 0.25, 0.25
  )

  // Spot Light
  const spotLight = new SpotLight(
    rgba(state.spotLight.color),
    state.spotLight.ambient,
    state.spotLight.diffuse,
    state.spotLight.specular,
    unormalize(state.spotLight.px, state.settings.gridSize, 0),
    unormalize(state.spotLight.py, state.settings.gridSize, 0),
    unormalize(state.spotLight.pz, state.settings.gridSize, 0),
    degToRad(state.spotLight.dx),
    degToRad(state.spotLight.dy),
    degToRad(state.spotLight.dz),
    state.spotLight.active,
    degToRad(state.spotLight.focus),
    degToRad(state.spotLight.cutOff),
    state.spotLight.constant,
    state.spotLight.linear,
    state.spotLight.quadratic
  )

  const spotLightOrbe = new Sphere()
  const spotLightOrbeMaterial = new Material(rgba(state.spotLight.color))
  const spotLightOrbeMesh = new Mesh(
    spotLightOrbe, spotLightOrbeMaterial, gl.TRIANGLES,
    state.spotLight.active,
    unormalize(state.spotLight.px, state.settings.gridSize, 0),
    unormalize(state.spotLight.py, state.settings.gridSize, 0),
    unormalize(state.spotLight.pz, state.settings.gridSize, 0),
    0, 0, 0, 0.25, 0.25, 0.25
  )

  // Grid setup
  const grid = new Grid(state.settings.gridSize, -0.01)
  const gridMaterial = new Material(rgba(state.settings.gridColor))
  const gridMesh = new Mesh(grid, gridMaterial, gl.LINES, state.settings.gridActive)

  // Axis x setup
  const xAxis = new AxisX(state.settings.gridSize / 2)
  const xAxisMaterial = new Material(rgba(state.settings.xAxisColor))
  const xAxisMesh = new Mesh(xAxis, xAxisMaterial, gl.LINES, state.settings.xAxisActive)

  // Axis y setup
  const yAxis = new AxisY(state.settings.gridSize / 2)
  const yAxisMaterial = new Material(rgba(state.settings.yAxisColor))
  const yAxisMesh = new Mesh(yAxis, yAxisMaterial, gl.LINES, state.settings.yAxisActive)

  // Axis z setup
  const zAxis = new AxisZ(state.settings.gridSize / 2)
  const zAxisMaterial = new Material(rgba(state.settings.zAxisColor))
  const zAxisMesh = new Mesh(zAxis, zAxisMaterial, gl.LINES, state.settings.zAxisActive)

  // Monkey setup
  const monkeySrc = require('./models/monkey.obj')
  const monkey = new WavefrontObj(monkeySrc, state.monkey.smooth)
  const monkeyMaterial = new Material(rgba(state.monkey.color), state.monkey.diffuse, state.monkey.specular, state.monkey.shininess, false)
  const monkeyMesh = new Mesh(
    monkey,
    monkeyMaterial,
    state.monkey.primitive,
    state.monkey.active,
    unormalize(state.monkey.tx, state.settings.gridSize, 0),
    unormalize(state.monkey.ty, state.settings.gridSize, 0),
    unormalize(state.monkey.tz, state.settings.gridSize, 0),
    degToRad(state.monkey.rx),
    degToRad(state.monkey.ry),
    degToRad(state.monkey.rz),
    unormalize(state.monkey.sx, state.settings.gridSize, 1),
    unormalize(state.monkey.sy, state.settings.gridSize, 1),
    unormalize(state.monkey.sz, state.settings.gridSize, 1)
  )

  // Teapot setup
  const teapotSrc = require('./models/teapot.obj')
  const teapot = new WavefrontObj(teapotSrc, state.teapot.smooth)
  const teapotMaterial = new Material(rgba(state.teapot.color), state.teapot.diffuse, state.teapot.specular, state.teapot.shininess, false)
  const teapotMesh = new Mesh(
    teapot,
    teapotMaterial,
    state.teapot.primitive,
    state.teapot.active,
    unormalize(state.teapot.tx, state.settings.gridSize, 0),
    unormalize(state.teapot.ty, state.settings.gridSize, 0),
    unormalize(state.teapot.tz, state.settings.gridSize, 0),
    degToRad(state.teapot.rx),
    degToRad(state.teapot.ry),
    degToRad(state.teapot.rz),
    unormalize(state.teapot.sx, state.settings.gridSize, 1),
    unormalize(state.teapot.sy, state.settings.gridSize, 1),
    unormalize(state.teapot.sz, state.settings.gridSize, 1)
  )

  // Scene
  const scene = new Scene(rgba(state.settings.backgroundColor), ambientLight)

  // Grid add on scene
  scene.addMesh(gridMesh)

  // Axis add on scene
  scene.addMesh(xAxisMesh)
  scene.addMesh(yAxisMesh)
  scene.addMesh(zAxisMesh)

  // Lights on scene
  scene.addDirLight(dirLight)
  scene.addPointLight(pointLightX)
  scene.addPointLight(pointLightY)
  scene.addPointLight(pointLightZ)
  scene.addSpotLight(spotLight)

  // Orbs add on scene
  scene.addMesh(pointLightXOrbeMesh)
  scene.addMesh(pointLightYOrbeMesh)
  scene.addMesh(pointLightZOrbeMesh)
  scene.addMesh(spotLightOrbeMesh)

  // Meshes add on scene
  scene.addMesh(monkeyMesh)
  scene.addMesh(teapotMesh)

  // Rendering scene
  const webGLInstance = new WebGLRenderer(canvas)
  webGLInstance.render(scene, camera) // (scene, camera)

  // Rendering loop
  window.requestAnimationFrame(render)
}

/*
// Mouse events
var mouse = require('mouse-event')
// Mouse event listener
window.addEventListener('mousemove', function (ev) {
  console.log(mouse.buttons(ev), mouse.x(ev), mouse.y(ev))
})
*/

// Forst call for rendering loop
window.requestAnimationFrame(render)
