const dat = require('dat.gui')

const guiControls = new dat.GUI()

const canvas = document.getElementById('c')
const gl = canvas.getContext('webgl')

const STATE = {
  settings: {
    backgroundColor: [51, 51, 51],
    xAxisActive: true,
    xAxisColor: [255, 0, 0],
    yAxisActive: true,
    yAxisColor: [0, 255, 0],
    zAxisActive: true,
    zAxisColor: [0, 0, 255],
    gridActive: true,
    gridColor: [225, 225, 225],
    gridSize: 12
  },
  camera: {
    perspective: true,
    centerX: 0,
    centerY: 0,
    centerZ: 0,
    eyeX: 5,
    eyeY: 5,
    eyeZ: 5,
    upX: 0,
    upY: 1,
    upZ: 0,
    fovy: 60,
    aspect: gl.canvas.width / gl.canvas.height,
    near: 0.001,
    far: 1000,
    left: -10,
    right: 10,
    bottom: -10,
    top: 10
  },
  ambientLight: {
    active: false,
    color: [255, 255, 255],
    ambient: 0.2
  },
  dirLight: {
    active: true,
    color: [255, 255, 255],
    ambient: 0.2,
    diffuse: 0.5,
    specular: 1.0,
    dx: -45,
    dy: -90,
    dz: -45
  },
  pointLightX: {
    active: false,
    color: [255, 0, 0],
    ambient: 0.2,
    diffuse: 0.5,
    specular: 1.0,
    px: 0.3,
    py: 0,
    pz: 0,
    constant: 1.0,
    linear: 0.09,
    quadratic: 0.032
  },
  pointLightY: {
    active: false,
    color: [0, 255, 0],
    ambient: 0.2,
    diffuse: 0.5,
    specular: 1.0,
    px: 0,
    py: 0.3,
    pz: 0,
    constant: 1.0,
    linear: 0.09,
    quadratic: 0.032
  },
  pointLightZ: {
    active: false,
    color: [0, 0, 255],
    ambient: 0.2,
    diffuse: 0.5,
    specular: 1.0,
    px: 0,
    py: 0,
    pz: 0.3,
    constant: 1.0,
    linear: 0.09,
    quadratic: 0.032
  },
  spotLight: {
    active: false,
    color: [255, 255, 0],
    ambient: 0.2,
    diffuse: 0.5,
    specular: 1.0,
    px: 0,
    py: 0.3,
    pz: 0.3,
    dx: 0,
    dy: -45,
    dz: -45,
    focus: 0.02,
    cutOff: 0.87,
    constant: 1.0,
    linear: 0.09,
    quadratic: 0.032
  },
  monkey: {
    active: true,
    smooth: true,
    primitive: gl.TRIANGLES,
    color: [225, 225, 225],
    diffuse: 0.5,
    specular: 1,
    shininess: 25,
    tx: 0,
    ty: 0,
    tz: 0,
    rx: 0,
    ry: 0,
    rz: 0,
    sx: 0,
    sy: 0,
    sz: 0
  },
  teapot: {
    active: false,
    smooth: true,
    primitive: gl.TRIANGLES,
    color: [225, 225, 225],
    diffuse: 0.5,
    specular: 1,
    shininess: 25,
    tx: 0,
    ty: 0,
    tz: 0,
    rx: 0,
    ry: 0,
    rz: 0,
    sx: 0,
    sy: 0,
    sz: 0
  }
}

let state = STATE

// Settings Folder
const settingsFolder = guiControls.addFolder('Global Settings')
settingsFolder.addColor(state.settings, 'backgroundColor')
settingsFolder.add(state.settings, 'xAxisActive')
settingsFolder.addColor(state.settings, 'xAxisColor')
settingsFolder.add(state.settings, 'yAxisActive')
settingsFolder.addColor(state.settings, 'yAxisColor')
settingsFolder.add(state.settings, 'zAxisActive')
settingsFolder.addColor(state.settings, 'zAxisColor')
settingsFolder.add(state.settings, 'gridActive')
settingsFolder.addColor(state.settings, 'gridColor')
settingsFolder.add(state.settings, 'gridSize', 0, 100, 2)

// Ambient Light Folder
const ambientLightFolder = guiControls.addFolder('Ambient Light')
ambientLightFolder.add(state.ambientLight, 'active')
ambientLightFolder.addColor(state.ambientLight, 'color')
ambientLightFolder.add(state.ambientLight, 'ambient', 0, 1, 0.001)

// Directional Light Folder
const dirLightFolder = guiControls.addFolder('Directional Light')
dirLightFolder.add(state.dirLight, 'active')
dirLightFolder.addColor(state.dirLight, 'color')
dirLightFolder.add(state.dirLight, 'ambient', 0, 1, 0.001)
dirLightFolder.add(state.dirLight, 'diffuse', 0, 1, 0.001)
dirLightFolder.add(state.dirLight, 'specular', 0, 1, 0.001)
dirLightFolder.add(state.dirLight, 'dx', -360, 360, 0.001)
dirLightFolder.add(state.dirLight, 'dy', -360, 360, 0.001)
dirLightFolder.add(state.dirLight, 'dz', -360, 360, 0.001)

// Point Light X
const pointLightXFolder = guiControls.addFolder('Point Light X')
pointLightXFolder.add(state.pointLightX, 'active')
pointLightXFolder.addColor(state.pointLightX, 'color')
pointLightXFolder.add(state.pointLightX, 'ambient', 0, 1, 0.001)
pointLightXFolder.add(state.pointLightX, 'diffuse', 0, 1, 0.001)
pointLightXFolder.add(state.pointLightX, 'specular', 0, 1, 0.001)
pointLightXFolder.add(state.pointLightX, 'px', -1, 1, 0.001)
pointLightXFolder.add(state.pointLightX, 'py', -1, 1, 0.001)
pointLightXFolder.add(state.pointLightX, 'pz', -1, 1, 0.001)
pointLightXFolder.add(state.pointLightX, 'constant', 0, 1, 0.001)
pointLightXFolder.add(state.pointLightX, 'linear', 0, 1, 0.001)
pointLightXFolder.add(state.pointLightX, 'quadratic', 0, 1, 0.001)

// Point Light Y
const pointLightYFolder = guiControls.addFolder('Point Light Y')
pointLightYFolder.add(state.pointLightY, 'active')
pointLightYFolder.addColor(state.pointLightY, 'color')
pointLightYFolder.add(state.pointLightY, 'ambient', 0, 1, 0.001)
pointLightYFolder.add(state.pointLightY, 'diffuse', 0, 1, 0.001)
pointLightYFolder.add(state.pointLightY, 'specular', 0, 1, 0.001)
pointLightYFolder.add(state.pointLightY, 'px', -1, 1, 0.001)
pointLightYFolder.add(state.pointLightY, 'py', -1, 1, 0.001)
pointLightYFolder.add(state.pointLightY, 'pz', -1, 1, 0.001)
pointLightYFolder.add(state.pointLightY, 'constant', 0, 1, 0.001)
pointLightYFolder.add(state.pointLightY, 'linear', 0, 1, 0.001)
pointLightYFolder.add(state.pointLightY, 'quadratic', 0, 1, 0.001)

// Point Light Z
const pointLightZFolder = guiControls.addFolder('Point Light Z')
pointLightZFolder.add(state.pointLightZ, 'active')
pointLightZFolder.addColor(state.pointLightZ, 'color')
pointLightZFolder.add(state.pointLightZ, 'ambient', 0, 1, 0.001)
pointLightZFolder.add(state.pointLightZ, 'diffuse', 0, 1, 0.001)
pointLightZFolder.add(state.pointLightZ, 'specular', 0, 1, 0.001)
pointLightZFolder.add(state.pointLightZ, 'px', -1, 1, 0.001)
pointLightZFolder.add(state.pointLightZ, 'py', -1, 1, 0.001)
pointLightZFolder.add(state.pointLightZ, 'pz', -1, 1, 0.001)
pointLightZFolder.add(state.pointLightZ, 'constant', 0, 1, 0.001)
pointLightZFolder.add(state.pointLightZ, 'linear', 0, 1, 0.001)
pointLightZFolder.add(state.pointLightZ, 'quadratic', 0, 1, 0.001)

// Spot Light
const spotLightFolder = guiControls.addFolder('Spot Light')
spotLightFolder.add(state.spotLight, 'active')
spotLightFolder.addColor(state.spotLight, 'color')
spotLightFolder.add(state.spotLight, 'ambient', 0, 1, 0.001)
spotLightFolder.add(state.spotLight, 'diffuse', 0, 1, 0.001)
spotLightFolder.add(state.spotLight, 'specular', 0, 1, 0.001)
spotLightFolder.add(state.spotLight, 'px', -1, 1, 0.001)
spotLightFolder.add(state.spotLight, 'py', -1, 1, 0.001)
spotLightFolder.add(state.spotLight, 'pz', -1, 1, 0.001)
spotLightFolder.add(state.spotLight, 'dx', -360, 360, 0.001)
spotLightFolder.add(state.spotLight, 'dy', -360, 360, 0.001)
spotLightFolder.add(state.spotLight, 'dz', -360, 360, 0.001)
spotLightFolder.add(state.spotLight, 'focus', -360, 360, 0.001)
spotLightFolder.add(state.spotLight, 'cutOff', -360, 360, 0.001)
spotLightFolder.add(state.spotLight, 'constant', 0, 1, 0.001)
spotLightFolder.add(state.spotLight, 'linear', 0, 1, 0.001)
spotLightFolder.add(state.spotLight, 'quadratic', 0, 1, 0.001)

// Camera Folder
const cameraFolder = guiControls.addFolder('Camera')
cameraFolder.add(state.camera, 'perspective')
cameraFolder.add(state.camera, 'centerX', -360, 360, 0.001)
cameraFolder.add(state.camera, 'centerY', -360, 360, 0.001)
cameraFolder.add(state.camera, 'centerZ', -360, 360, 0.001)
cameraFolder.add(state.camera, 'eyeX', -360, 360, 0.001)
cameraFolder.add(state.camera, 'eyeY', -360, 360, 0.001)
cameraFolder.add(state.camera, 'eyeZ', -360, 360, 0.001)
cameraFolder.add(state.camera, 'upX', -360, 360, 0.001)
cameraFolder.add(state.camera, 'upY', -360, 360, 0.001)
cameraFolder.add(state.camera, 'upZ', -360, 360, 0.001)

cameraFolder.add(state.camera, 'fovy', -180, 180, 0.001)
cameraFolder.add(state.camera, 'aspect', 0, 2 * gl.canvas.width / gl.canvas.height, 0.001)

cameraFolder.add(state.camera, 'left', -100, 100, 0.001)
cameraFolder.add(state.camera, 'right', -100, 100, 0.001)
cameraFolder.add(state.camera, 'bottom', -100, 100, 0.001)
cameraFolder.add(state.camera, 'top', -100, 100, 0.001)

cameraFolder.add(state.camera, 'near', 0, 1000, 0.001)
cameraFolder.add(state.camera, 'far', 0, 1000, 0.001)

// Monkey Folder
const monkeyFolder = guiControls.addFolder('Monkey')
monkeyFolder.add(state.monkey, 'active')
monkeyFolder.add(state.monkey, 'smooth')
monkeyFolder.add(state.monkey, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
monkeyFolder.addColor(state.monkey, 'color')
monkeyFolder.add(state.monkey, 'diffuse', 0, 1, 0.001)
monkeyFolder.add(state.monkey, 'specular', 0, 1, 0.001)
monkeyFolder.add(state.monkey, 'shininess', 1, 128, 0.001)
monkeyFolder.add(state.monkey, 'tx', -1, 1, 0.001)
monkeyFolder.add(state.monkey, 'ty', -1, 1, 0.001)
monkeyFolder.add(state.monkey, 'tz', -1, 1, 0.001)
monkeyFolder.add(state.monkey, 'rx', 0, 360, 0.1)
monkeyFolder.add(state.monkey, 'ry', 0, 360, 0.1)
monkeyFolder.add(state.monkey, 'rz', 0, 360, 0.1)
monkeyFolder.add(state.monkey, 'sx', -1, 1, 0.001)
monkeyFolder.add(state.monkey, 'sy', -1, 1, 0.001)
monkeyFolder.add(state.monkey, 'sz', -1, 1, 0.001)

// Teapot Folder
const teapotFolder = guiControls.addFolder('Teapot')
teapotFolder.add(state.teapot, 'active')
teapotFolder.add(state.teapot, 'smooth')
teapotFolder.add(state.teapot, 'primitive', { Triangles: gl.TRIANGLES, Lines: gl.LINES })
teapotFolder.addColor(state.teapot, 'color')
teapotFolder.add(state.teapot, 'diffuse', 0, 1, 0.001)
teapotFolder.add(state.teapot, 'specular', 0, 1, 0.001)
teapotFolder.add(state.teapot, 'shininess', 1, 128, 0.001)
teapotFolder.add(state.teapot, 'tx', -1, 1, 0.001)
teapotFolder.add(state.teapot, 'ty', -1, 1, 0.001)
teapotFolder.add(state.teapot, 'tz', -1, 1, 0.001)
teapotFolder.add(state.teapot, 'rx', 0, 360, 0.1)
teapotFolder.add(state.teapot, 'ry', 0, 360, 0.1)
teapotFolder.add(state.teapot, 'rz', 0, 360, 0.1)
teapotFolder.add(state.teapot, 'sx', -1, 1, 0.001)
teapotFolder.add(state.teapot, 'sy', -1, 1, 0.001)
teapotFolder.add(state.teapot, 'sz', -1, 1, 0.001)

module.exports = {
  state
}
