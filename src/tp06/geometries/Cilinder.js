const Geometry = require('./Geometry')
const RegularConvexPolygonGeometry = require('./RegularConvexPolygonGeometry')

class Cylinder extends Geometry {
  constructor (edges = 30, height = 1) {
    const circleBottom = new RegularConvexPolygonGeometry(edges, -height / 2)
    const circleTop = new RegularConvexPolygonGeometry(edges, height / 2)

    let vertices = circleBottom.vertices
    vertices.push(...circleTop.vertices)

    let faces = circleBottom.faces
    for (let i = 0; i < circleTop.faces.length; i++) {
      circleTop.faces[i] = circleTop.faces[i] + edges + 1
    }
    faces.push(...circleTop.faces)

    for (let i = 0; i < edges; i++) {
      faces.push((i + 1) % edges, i, (edges + 1) + ((edges - i) % edges))
      faces.push((edges + 1) + ((edges - i) % edges), (edges + 1) + ((edges - i - 1) % edges), (i + 1) % edges)
    }

    super(vertices, faces)
  }
}

module.exports = Cylinder
