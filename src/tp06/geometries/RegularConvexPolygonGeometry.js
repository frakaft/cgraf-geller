const Geometry = require('./Geometry')

class RegularConvexPolygonGeometry extends Geometry {
  constructor (edges = 3, zShift = 0, size = 1) {
    let vertices = []
    const normal = zShift / Math.abs(zShift)
    const angle = (2 * Math.PI) / edges
    for (let i = 0; i < edges; i++) {
      vertices.push(Math.cos(angle * ((edges + (i * normal)) % edges)) * size)
      vertices.push(Math.sin(angle * ((edges + (i * normal)) % edges)) * size)
      vertices.push(zShift)
    }
    vertices.push(0)
    vertices.push(0)
    vertices.push(zShift)

    let faces = []
    for (let i = 0; i < edges; i++) {
      faces.push(edges)
      faces.push(i)
      faces.push((i + 1) % (edges))
    }

    super(vertices, faces)
  }
}

module.exports = RegularConvexPolygonGeometry
