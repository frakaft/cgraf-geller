const Geometry = require('./Geometry')
const { normalize, normals } = require('../utils/vec3')

class WavefrontObj extends Geometry {
  constructor (src, smooth) {
    const json = parseObj(src, smooth)
    super(json.vertices, json.faces, json.normals)
  }
}

module.exports = WavefrontObj

function parseObj (src, smooth) {
  smooth = smooth || false

  let lines = src.split('\n')
  let vlines = lines.filter(
    function (l) { return l[0] === 'v' }
  )
  let flines = lines.filter(
    function (l) { return l[0] === 'f' }
  )

  let verts = new Array(3 * vlines.length)

  for (let i = 0; i < vlines.length; ++i) {
    let posStr = vlines[i].split(' ')
    verts[3 * i + 0] = parseFloat(posStr[1])
    verts[3 * i + 1] = parseFloat(posStr[2])
    verts[3 * i + 2] = parseFloat(posStr[3])
  }

  let pos = new Array(3 * 3 * flines.length)
  let idx = new Array(3 * flines.length)
  let normalsv = new Array(3 * 3 * flines.length)
  let normalsAvg = new Array(3 * vlines.length)

  for (let i = 0; i < normalsAvg.length; ++i) {
    normalsAvg[i] = 0.0
  }

  for (let i = 0; i < flines.length; ++i) {
    let triStr = flines[i].split(' ')

    let idx0 = parseInt(triStr[1]) - 1
    let pos0 = verts.slice(idx0 * 3, idx0 * 3 + 3)
    pos[(i * 3 + 0) * 3 + 0] = pos0[0]
    pos[(i * 3 + 0) * 3 + 1] = pos0[1]
    pos[(i * 3 + 0) * 3 + 2] = pos0[2]
    idx[i * 3 + 0] = i * 3 + 0

    let idx1 = parseInt(triStr[2]) - 1
    let pos1 = verts.slice(idx1 * 3, idx1 * 3 + 3)
    pos[(i * 3 + 1) * 3 + 0] = pos1[0]
    pos[(i * 3 + 1) * 3 + 1] = pos1[1]
    pos[(i * 3 + 1) * 3 + 2] = pos1[2]
    idx[i * 3 + 1] = i * 3 + 1

    let idx2 = parseInt(triStr[3]) - 1
    let pos2 = verts.slice(idx2 * 3, idx2 * 3 + 3)
    pos[(i * 3 + 2) * 3 + 0] = pos2[0]
    pos[(i * 3 + 2) * 3 + 1] = pos2[1]
    pos[(i * 3 + 2) * 3 + 2] = pos2[2]
    idx[i * 3 + 2] = i * 3 + 2

    let n = normals(pos0, pos1, pos2)

    if (smooth) {
      normalsAvg[idx0 * 3 + 0] += n[0]
      normalsAvg[idx0 * 3 + 1] += n[1]
      normalsAvg[idx0 * 3 + 2] += n[2]
      normalsAvg[idx1 * 3 + 0] += n[0]
      normalsAvg[idx1 * 3 + 1] += n[1]
      normalsAvg[idx1 * 3 + 2] += n[2]
      normalsAvg[idx2 * 3 + 0] += n[0]
      normalsAvg[idx2 * 3 + 1] += n[1]
      normalsAvg[idx2 * 3 + 2] += n[2]
    } else {
      normalsv[(i * 3 + 0) * 3 + 0] = n[0]
      normalsv[(i * 3 + 0) * 3 + 1] = n[1]
      normalsv[(i * 3 + 0) * 3 + 2] = n[2]
      normalsv[(i * 3 + 1) * 3 + 0] = n[0]
      normalsv[(i * 3 + 1) * 3 + 1] = n[1]
      normalsv[(i * 3 + 1) * 3 + 2] = n[2]
      normalsv[(i * 3 + 2) * 3 + 0] = n[0]
      normalsv[(i * 3 + 2) * 3 + 1] = n[1]
      normalsv[(i * 3 + 2) * 3 + 2] = n[2]
    }
  }

  if (smooth) {
    for (let i = 0; i < flines.length; ++i) {
      let triStr = flines[i].split(' ')

      let idx0 = parseInt(triStr[1]) - 1
      let idx1 = parseInt(triStr[2]) - 1
      let idx2 = parseInt(triStr[3]) - 1

      let n0 = normalize(normalsAvg.slice(3 * idx0, 3 * idx0 + 3))
      let n1 = normalize(normalsAvg.slice(3 * idx1, 3 * idx1 + 3))
      let n2 = normalize(normalsAvg.slice(3 * idx2, 3 * idx2 + 3))

      normalsv[(i * 3 + 0) * 3 + 0] = n0[0]
      normalsv[(i * 3 + 0) * 3 + 1] = n0[1]
      normalsv[(i * 3 + 0) * 3 + 2] = n0[2]
      normalsv[(i * 3 + 1) * 3 + 0] = n1[0]
      normalsv[(i * 3 + 1) * 3 + 1] = n1[1]
      normalsv[(i * 3 + 1) * 3 + 2] = n1[2]
      normalsv[(i * 3 + 2) * 3 + 0] = n2[0]
      normalsv[(i * 3 + 2) * 3 + 1] = n2[1]
      normalsv[(i * 3 + 2) * 3 + 2] = n2[2]
    }
  }

  return {
    vertices: pos,
    faces: idx,
    normals: normalsv,
    mode: 'TRIANGLES'
  }
}
