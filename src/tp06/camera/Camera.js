const glMatrix = require('gl-matrix')

class Camera {
  constructor () {
    this.projectionMatrix = undefined
    this.eyeX = undefined
    this.eyeY = undefined
    this.eyeZ = undefined
    this.centerX = undefined
    this.centerY = undefined
    this.centerZ = undefined
    this.upX = undefined
    this.upY = undefined
    this.upZ = undefined
    this.viewMatrix = glMatrix.mat4.create()
    this.iViewMatrix = glMatrix.mat4.create()
  }
  getViewMatrix () {
    glMatrix.mat4.identity(this.viewMatrix)
    glMatrix.mat4.lookAt(this.viewMatrix, [this.eyeX, this.eyeY, this.eyeZ], [this.centerX, this.centerY, this.centerZ], [this.upX, this.upY, this.upZ])
    return this.viewMatrix
  }
  getViewPos () {
    glMatrix.mat4.invert(this.iViewMatrix, this.viewMatrix)
    return [this.iViewMatrix[12], this.iViewMatrix[13], this.iViewMatrix[14]]
  }
}

module.exports = Camera
