const Camera = require('./Camera')
const glMatrix = require('gl-matrix')

class OrthographicCamera extends Camera {
  constructor (left, right, bottom, top, near, far) {
    super()
    this.left = left
    this.right = right
    this.bottom = bottom
    this.top = top
    this.near = near
    this.far = far
    this.projectionMatrix = glMatrix.mat4.create()
  }
  getProjectionMatrix () {
    glMatrix.mat4.identity(this.projectionMatrix)
    glMatrix.mat4.ortho(this.projectionMatrix, this.left, this.right, this.bottom, this.top, this.near, this.far)
    return this.projectionMatrix
  }
}

module.exports = OrthographicCamera
